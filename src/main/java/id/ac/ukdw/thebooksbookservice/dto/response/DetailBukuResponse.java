package id.ac.ukdw.thebooksbookservice.dto.response;

import lombok.Data;

import java.util.List;

@Data
public class DetailBukuResponse {

    private String isbn;

    private String judulBuku;

    private List<String> namaPengarang;

    private List<String> genreBuku;

    private int jumlahHalaman;

    private byte[] coverBuku;

    private byte[] sampleBuku;

    private String deskripsiBuku;

    private String tahunTerbit;

    private int stockBuku;

    private double hargaBuku;

    private String namaPenerbit;

    public DetailBukuResponse(){}

    public  DetailBukuResponse(
            String isbn,
            String judulBuku,
            List<String> namaPengarang,
            List<String> genreBuku,
            int jumlahHalaman,
            byte[] coverBuku,
            byte[] sampleBuku,
            String deskripsiBuku,
            String tahunTerbit,
            int stockBuku,
            double hargaBuku,
            String namaPenerbit){

        this.isbn = isbn;
        this.judulBuku = judulBuku;
        this.namaPengarang = namaPengarang;
        this.genreBuku = genreBuku;
        this.jumlahHalaman = jumlahHalaman;
        this.coverBuku = coverBuku;
        this.sampleBuku = sampleBuku;
        this.deskripsiBuku = deskripsiBuku;
        this.tahunTerbit = tahunTerbit;
        this.stockBuku = stockBuku;
        this.hargaBuku = hargaBuku;
        this.namaPenerbit = namaPenerbit;


    }
}
