package id.ac.ukdw.thebooksbookservice.dto.response;

import lombok.Data;

import java.util.List;

@Data
public class BukuResponse {

    private String isbn;

    private String judulBuku;

    private List<String> namaPengarang;

    private byte[] coverBuku;

    private int stockBuku;

    private double hargaBuku;

    public BukuResponse(){}

    public BukuResponse(String isbn, 
                        String judulBuku,
                        List<String> namaPengarang,
                        byte[] coverBuku,
                        int stockBuku,
                        double hargaBuku){
        
        this.isbn = isbn;
        this.judulBuku = judulBuku;
        this.namaPengarang = namaPengarang;
        this.stockBuku = stockBuku;
        this.hargaBuku = hargaBuku;
        this.coverBuku = coverBuku;
    }

}
