package id.ac.ukdw.thebooksbookservice.dto;

import lombok.Data;

import javax.persistence.Column;

@Data
public class ListGenreBukuDTO {

    private int id;

    private String idGenre;

    private String isbn;
}
