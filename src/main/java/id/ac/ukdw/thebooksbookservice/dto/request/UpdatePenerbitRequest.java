package id.ac.ukdw.thebooksbookservice.dto.request;

import lombok.Data;

@Data
public class UpdatePenerbitRequest {

    private String namaPenerbit;

    public UpdatePenerbitRequest(){}

    public UpdatePenerbitRequest(
            String namaPenerbit){

        this.namaPenerbit = namaPenerbit;
    }
}
