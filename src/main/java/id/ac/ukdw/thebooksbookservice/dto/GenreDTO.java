package id.ac.ukdw.thebooksbookservice.dto;

import lombok.Data;

@Data
public class GenreDTO {
    
    private String idGenre;
    
    private String namaGenre;
    
}
