package id.ac.ukdw.thebooksbookservice.dto.request;

import lombok.Data;

@Data
public class PenerbitRequest {

    private String idPenerbit;

    private String namaPenerbit;

    public PenerbitRequest(){}

    public PenerbitRequest(
            String idPenerbit,
            String namaPenerbit){

        this.idPenerbit = idPenerbit;
        this.namaPenerbit = namaPenerbit;
    }
}
