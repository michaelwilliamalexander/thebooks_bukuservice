package id.ac.ukdw.thebooksbookservice.dto.request;

import lombok.Data;

@Data
public class GenreRequest {

    private String idGenre;

    private String namaGenre;

    public GenreRequest(){}

    public GenreRequest(String idGenre, String namaGenre){
        this.idGenre = idGenre;
        this.namaGenre = namaGenre;
    }
}
