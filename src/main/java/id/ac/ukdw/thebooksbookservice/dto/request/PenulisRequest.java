package id.ac.ukdw.thebooksbookservice.dto.request;

import lombok.Data;

@Data
public class PenulisRequest {

    private String idPenulis;

    private String namaPenulis;

    public PenulisRequest(){}

    public PenulisRequest(
            String idPenulis,
            String namaPenulis){
        
        this.idPenulis = idPenulis;
        this.namaPenulis = namaPenulis;
    }
}
