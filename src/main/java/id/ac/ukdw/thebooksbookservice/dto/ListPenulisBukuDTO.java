package id.ac.ukdw.thebooksbookservice.dto;

import lombok.Data;

import javax.persistence.Column;

@Data
public class ListPenulisBukuDTO {

    private int id;
    
    private String isbn;
    
    private String idPenulis;
}
