package id.ac.ukdw.thebooksbookservice.dto.request;

import lombok.Data;

@Data
public class UpdatePenulisRequest {

    private String namaPenulis;

    public UpdatePenulisRequest(){}

    public UpdatePenulisRequest(
            String namaPenulis){

        this.namaPenulis = namaPenulis;
    }
}
