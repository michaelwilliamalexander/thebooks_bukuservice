package id.ac.ukdw.thebooksbookservice.dto.response;

import lombok.Data;

@Data
public class ResponseWrapper {

    private Object data;

    public ResponseWrapper() {

    }

    /**
     * @param data
     */
    public ResponseWrapper(Object data) {
        this.data = data;
    }

}
