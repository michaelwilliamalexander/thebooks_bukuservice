package id.ac.ukdw.thebooksbookservice.dto;

import id.ac.ukdw.thebooksbookservice.model.Penerbit;
import lombok.Data;

@Data
public class BukuDTO {

    private String isbn;

    private String judulBuku;

    private int stockBuku;

    private double hargaBuku;

    private String deskripsiBuku;

    private byte[] coverBuku;

    private byte[] sampleBuku;
    
    private int jumlahHalaman;
    
    private String tahunTerbit;

    private Penerbit penerbit;
}
