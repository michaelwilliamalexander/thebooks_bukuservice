package id.ac.ukdw.thebooksbookservice.dto.request;

import lombok.Data;

@Data
public class UpdateGenreRequest {

    private String namaGenre;

    public UpdateGenreRequest(){}

    public UpdateGenreRequest(String namaGenre){
        this.namaGenre = namaGenre;
    }
}
