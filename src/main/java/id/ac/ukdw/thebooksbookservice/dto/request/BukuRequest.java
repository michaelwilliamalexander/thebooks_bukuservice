package id.ac.ukdw.thebooksbookservice.dto.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
public class BukuRequest {

    private String isbn;

    private String judulBuku;

    private String deskripsiBuku;

    private String tahunTerbit;

    private int jumlahHalaman;

    private int stockBuku;

    private double hargaBuku;

    private List<String> idGenre;

    private List<String> idPenulis;

    private String idPenerbit;

    public BukuRequest(){}

    public BukuRequest(
            String isbn,
            String judulBuku,
            String deskripsiBuku,
            String tahunTerbit,
            int stockBuku,
            double hargaBuku,
            int jumlahHalaman,
            List<String> idGenre,
            List<String> idPenulis,
            String idPenerbit
    ){
        this.isbn = isbn;
        this.judulBuku = judulBuku;
        this.deskripsiBuku = deskripsiBuku;
        this.tahunTerbit = tahunTerbit;
        this.hargaBuku = hargaBuku;
        this.jumlahHalaman = jumlahHalaman;
        this.stockBuku = stockBuku;
        this.idGenre = idGenre;
        this.idPenulis = idPenulis;
        this.idPenerbit = idPenerbit;
    }


}
