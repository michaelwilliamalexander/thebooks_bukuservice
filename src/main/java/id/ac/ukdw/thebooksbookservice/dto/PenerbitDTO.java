package id.ac.ukdw.thebooksbookservice.dto;

import lombok.Data;

@Data
public class PenerbitDTO {

    private String idPenerbit;

    private String namaPenerbit;

}
