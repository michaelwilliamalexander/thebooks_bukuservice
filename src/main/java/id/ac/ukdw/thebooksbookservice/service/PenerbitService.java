package id.ac.ukdw.thebooksbookservice.service;

import id.ac.ukdw.thebooksbookservice.dto.PenerbitDTO;
import id.ac.ukdw.thebooksbookservice.exception.BadRequestException;
import id.ac.ukdw.thebooksbookservice.exception.NotFoundException;
import id.ac.ukdw.thebooksbookservice.model.Penerbit;
import id.ac.ukdw.thebooksbookservice.repository.dao.PenerbitDao;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.valid4j.Validation.validate;

@Service
@RequiredArgsConstructor
public class PenerbitService {

    private final ModelMapper mapper;

    private final PenerbitDao penerbitRepository;

    public String addPenerbit(String idPenerbit, String namaPenerbit){
        validate(!idPenerbit.isEmpty(), new BadRequestException());
        validate(idPenerbit !=null, new BadRequestException());
        validate(!namaPenerbit.isEmpty(), new BadRequestException());
        validate(namaPenerbit!=null, new BadRequestException());

        Optional<Penerbit> findPenerbit = penerbitRepository.findById(idPenerbit);
        Optional<Penerbit> findNamaPenerbit = penerbitRepository.findByNamaPenerbit(namaPenerbit);
        if(findPenerbit.isEmpty() && findNamaPenerbit.isEmpty()){
            Penerbit penerbit = new Penerbit(idPenerbit, namaPenerbit);
            if(penerbitRepository.save(penerbit)) {
                return "Penyimpanan Berhasil";
            }
        }
        throw new BadRequestException();
    }

    public String updatePenerbit(String idPenerbit, String namaPenerbit){
        validate(!idPenerbit.isEmpty(), new BadRequestException());
        validate(idPenerbit !=null, new BadRequestException());
        validate(!namaPenerbit.isEmpty(), new BadRequestException());
        validate(namaPenerbit!=null, new BadRequestException());

        Optional<Penerbit> findPenerbit = penerbitRepository.findById(idPenerbit);
        Optional<Penerbit> findNamaPenerbit = penerbitRepository.findByNamaPenerbit(namaPenerbit);
        if (findPenerbit.isPresent() && findNamaPenerbit.isEmpty()){
            Penerbit penerbit = new Penerbit(idPenerbit,namaPenerbit);
            if (penerbitRepository.update(penerbit)) return "Update Berhasil";
        }
        throw new BadRequestException();
    }

    public List<PenerbitDTO> getAllPenerbit(){
        Optional<List<Penerbit>> findData = penerbitRepository.getAll();
        if (findData.isPresent()){
            List<PenerbitDTO> result = new ArrayList<>();
            for(Penerbit penerbit:findData.get()){
                result.add(mapper.map(penerbit,PenerbitDTO.class));
            }
            return result;
        }
        throw new NotFoundException();
    }

    public String deletePenerbit(String idPenerbit){
        Optional<Penerbit> penerbit = penerbitRepository.findById(idPenerbit);
        if(penerbit.isPresent()){
            if(penerbitRepository.delete(idPenerbit)) return "Delete Berhasil";
        }
        throw new BadRequestException();
    }

}
