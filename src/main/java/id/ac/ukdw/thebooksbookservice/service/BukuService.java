package id.ac.ukdw.thebooksbookservice.service;

import id.ac.ukdw.thebooksbookservice.dto.response.BukuResponse;
import id.ac.ukdw.thebooksbookservice.dto.response.DetailBukuResponse;
import id.ac.ukdw.thebooksbookservice.exception.BadRequestException;
import id.ac.ukdw.thebooksbookservice.exception.NotFoundException;
import id.ac.ukdw.thebooksbookservice.model.*;
import id.ac.ukdw.thebooksbookservice.repository.dao.*;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.valid4j.Validation.validate;

@Service
@RequiredArgsConstructor
public class BukuService {

    private final PenulisDao penulisRepository;

    private final PenerbitDao penerbitRepository;

    private final GenreDao genreRepository;

    private final BukuDao bukuRepository;

    private final ListGenreBukuDao listGenreBukuRepository;

    private final ListPenulisDao listPenulisBukuRepository;

    private final ModelMapper mapper;

    public List<BukuResponse> getAllBuku(){
        List<BukuResponse> dataResult = new ArrayList<>();
        Optional<List<Buku>> bukuList = bukuRepository.getAll();
        if (bukuList.isPresent()){
           for (Buku buku:bukuList.get()){
               Optional<List<ListPenulisBuku>> listPenulisBuku =
                       listPenulisBukuRepository.findByIsbn(buku.getIsbn());
               if (listPenulisBuku.isPresent()){
                   for (ListPenulisBuku dataPenulis:listPenulisBuku.get()){
                       List<String> namaPenulis = new ArrayList<>();
                       Optional<Penulis> penulis = penulisRepository.findById(dataPenulis.getIdPenulis());
                       if ( penulis.isPresent()){
                           namaPenulis.add(penulis.get().getNamaPenulis());
                       }
                       dataResult.add(new BukuResponse(
                               buku.getIsbn(),
                               buku.getJudulBuku(),
                               namaPenulis,
                               buku.getCoverBuku(),
                               buku.getStockBuku(),
                               buku.getHargaBuku()
                       ));
                   }
               }

           }
           return dataResult;
        }
        throw new NotFoundException();
    }

    public String saveBuku(
            String isbn,
            String judulBuku,
            String deskripsiBuku,
            double hargaBuku,
            int stockBuku,
            int jumlahHalaman,
            String tahunTerbit,
            List<String> idGenre,
            List<String> idPenulis,
            String idPenerbit,
            byte[] coverBuku,
            byte[] sampleBuku){
        validate(!isbn.isEmpty(), new BadRequestException());
        validate(isbn !=null, new BadRequestException());
        validate(!judulBuku.isEmpty(), new BadRequestException());
        validate(judulBuku!=null, new BadRequestException());

        Optional<Buku> findBuku = bukuRepository.findById(isbn);
        Optional<Penerbit> penerbit = penerbitRepository.findById(idPenerbit);
        if(findBuku.isEmpty() && penerbit.isPresent()){
            Buku buku = new Buku(
                isbn,
                judulBuku,
                deskripsiBuku,
                jumlahHalaman,
                tahunTerbit,
                hargaBuku,
                stockBuku,
                coverBuku,
                sampleBuku,
                penerbit.get()
            );
            bukuRepository.save(buku);
            if(checkDataGenre(idGenre).isPresent()){
                List<Genre> dataGenre = checkDataGenre(idGenre).get();
                for (Genre genre: dataGenre){
                    listGenreBukuRepository.save(
                            new ListGenreBuku(genre.getIdGenre(),isbn)
                    );
                }
            }
            if(checkDataPenulis(idPenulis).isPresent()){
                List<Penulis> dataPenulis = checkDataPenulis(idPenulis).get();
                for(Penulis penulis: dataPenulis){
                    listPenulisBukuRepository.save(
                        new ListPenulisBuku(penulis.getIdPenulis(),isbn)
                    );
                }
            }
            return "Penyimpanan Berhasil";
        }
        throw new BadRequestException();
    }

    private Optional<List<Genre>> checkDataGenre(List<String> idGenre){
        List<Genre> result = new ArrayList<>();
        for (String id: idGenre){
            Optional<Genre> genre = genreRepository.findById(id);
            if (genre.isPresent()){
                result.add(genre.get());
            }
        }
        return Optional.ofNullable(result);
    }

    private Optional<List<Penulis>> checkDataPenulis(List<String> idPenulis){
        List<Penulis> result = new ArrayList<>();
        for(String id: idPenulis){
            Optional<Penulis> penulis = penulisRepository.findById(id);
            if(penulis.isPresent()){
                result.add(penulis.get());
            }
        }
        return Optional.ofNullable(result);
    }

    public String deleteBuku(String isbn){
        Optional<Buku> findBuku = bukuRepository.findById(isbn);
        Optional<List<ListGenreBuku>> findGenre = listGenreBukuRepository.findByIsbn(isbn);
        Optional<List<ListPenulisBuku>> findPenulis = listPenulisBukuRepository.findByIsbn(isbn);
        if (findBuku.isPresent()){
            if (findGenre.isPresent()){
                for (ListGenreBuku data:findGenre.get()){
                    listGenreBukuRepository.delete(data.getId());
                }
            }
            if (findPenulis.isPresent()){
                for (ListPenulisBuku data:findPenulis.get()){
                    listPenulisBukuRepository.delete(data.getId());
                }
            }
            bukuRepository.delete(isbn);
            return "Delete Berhasil";
        }
        throw new BadRequestException();
    }

    public String updateBuku(
            String isbn,
            String judulBuku,
            String deskripsiBuku,
            double hargaBuku,
            int stockBuku,
            int jumlahHalaman,
            String tahunTerbit,
            List<String> idGenre,
            List<String> idPenulis,
            String idPenerbit,
            byte[] coverBuku,
            byte[] sampleBuku){
        validate(!isbn.isEmpty(), new BadRequestException());
        validate(isbn !=null, new BadRequestException());
        validate(!judulBuku.isEmpty(), new BadRequestException());
        validate(judulBuku!=null, new BadRequestException());

        Optional<Buku> findBuku = bukuRepository.findById(isbn);
        Optional<Penerbit> penerbit = penerbitRepository.findById(idPenerbit);
        if(findBuku.isPresent() && penerbit.isPresent()){
            Buku buku = new Buku(
                    isbn,
                    judulBuku,
                    deskripsiBuku,
                    jumlahHalaman,
                    tahunTerbit,
                    hargaBuku,
                    stockBuku,
                    coverBuku,
                    sampleBuku,
                    penerbit.get()
            );
            bukuRepository.update(buku);
            Optional<List<ListGenreBuku>> findGenre = listGenreBukuRepository.findByIsbn(isbn);
            Optional<List<ListPenulisBuku>> findPenulis = listPenulisBukuRepository.findByIsbn(isbn);
            if (checkDataGenre(idGenre).isPresent()){
                if (findGenre.isPresent()){
                    updateGenre(findGenre,checkDataGenre(idGenre),isbn);
                }else{
                    for (Genre genre:checkDataGenre(idGenre).get()){
                        listGenreBukuRepository.save(new ListGenreBuku(genre.getIdGenre(),isbn));
                    }
                }
            }
            if (checkDataPenulis(idPenulis).isPresent()){
                if (findPenulis.isPresent()){
                    updatePenulis(findPenulis,checkDataPenulis(idPenulis),isbn);
                }else{
                    for(Penulis penulis:checkDataPenulis(idPenulis).get()){
                        listPenulisBukuRepository.save(
                                new ListPenulisBuku(penulis.getIdPenulis(), isbn)
                        );
                    }
                }

            }
            return "update buku berhasil";
        }
        throw new BadRequestException();
    }

    private void updateGenre(Optional<List<ListGenreBuku>> findGenre,
                            Optional<List<Genre>> dataGenre, String isbn){
        if (findGenre.isPresent()){
            for (ListGenreBuku item:findGenre.get()){
                for (Genre genre: dataGenre.get()){
                    Optional<ListGenreBuku> getGenre =
                            listGenreBukuRepository.findByIdGenreAndIsbn(genre.getIdGenre(),isbn);
                    if ( !getGenre.isPresent() && !item.getIdGenre().equals(genre.getIdGenre())){
                        listGenreBukuRepository.delete(item.getId());
                        listGenreBukuRepository.save(new ListGenreBuku(genre.getIdGenre(),isbn));
                    }
                }
            }
        }
    }

    private void updatePenulis(Optional<List<ListPenulisBuku>> findPenulis,
                              Optional<List<Penulis>> dataPenulis, String isbn){
        if (findPenulis.isPresent()){
            for (ListPenulisBuku item:findPenulis.get()){
                for(Penulis penulis: dataPenulis.get()){
                    Optional<ListPenulisBuku> getPenulis =
                            listPenulisBukuRepository.findByIdPenulisAndIsbn(penulis.getIdPenulis(),isbn);
                    if (!getPenulis.isPresent() && !item.getIdPenulis().equals(penulis.getIdPenulis())){
                        listPenulisBukuRepository.delete(item.getId());
                        listPenulisBukuRepository.save(new ListPenulisBuku(penulis.getIdPenulis(), isbn));
                    }
                }
            }
        }
    }

    public DetailBukuResponse getDetailBuku(String isbn){
        Optional<Buku> findBuku = bukuRepository.findById(isbn);
        Optional<List<ListGenreBuku>> findGenre = listGenreBukuRepository.findByIsbn(isbn);
        Optional<List<ListPenulisBuku>> findPenulis = listPenulisBukuRepository.findByIsbn(isbn);

        if (findBuku.isPresent()){
            List<String> penulis = new ArrayList<>();
            List<String> genre = new ArrayList<>();

            for (ListPenulisBuku item : findPenulis.get()){
                Optional<Penulis> dataResult = penulisRepository.findById(item.getIdPenulis());
                if (dataResult.isPresent()){
                    penulis.add(dataResult.get().getNamaPenulis());
              }
            }

            for(ListGenreBuku item : findGenre.get()){
                Optional<Genre> data = genreRepository.findById(item.getIdGenre());
                if(data.isPresent()){
                    genre.add(data.get().getNamaGenre());
                }
            }

            Buku buku = findBuku.get();
            DetailBukuResponse detail = new DetailBukuResponse(
                 buku.getIsbn(),
                 buku.getJudulBuku(),
                 penulis,
                   genre,
                 buku.getJumlahHalaman(),
                 buku.getCoverBuku(),
                 buku.getSampleBuku(),
                 buku.getDeskripsiBuku(),
                 buku.getTahunTerbit(),
                 buku.getStockBuku(),
                 buku.getHargaBuku(),
                 buku.getPenerbit().getNamaPenerbit()
            );
            return detail;
        }
        throw new NotFoundException();
    }

    public List<BukuResponse> findBukuByIdGenre(String idGenre){
        Optional<List<ListGenreBuku>> listGenreBuku =
                listGenreBukuRepository.findByIdGenre(idGenre);
        List<BukuResponse> listBukuByGenre = new ArrayList<>();
        if(listGenreBuku.isPresent()){
            return getBukuResponses(listGenreBuku.get());
        }
        throw new NotFoundException();
    }

    private List<BukuResponse> getBukuResponses(List<ListGenreBuku> listGenreBuku) {
        List<BukuResponse> finalData = new ArrayList<>();
        for(ListGenreBuku item : listGenreBuku){
            Optional<Buku> data = bukuRepository.findById(item.getIsbn());
            List<String> namaPenulis = getDataBukuPenulis(item.getIsbn(), data.get());
            BukuResponse buku = new BukuResponse(
                data.get().getIsbn(),
                data.get().getJudulBuku(),
                namaPenulis,
                data.get().getCoverBuku(),
                data.get().getStockBuku(),
                data.get().getHargaBuku()
            );
            finalData.add(buku);
        }
        return finalData;
    }

    public List<BukuResponse> searchBuku(String keyword){
        List<BukuResponse> finalData = new ArrayList<>();
        Optional<List<Buku>> dataBuku = bukuRepository.searchBuku(keyword);
        Optional<List<Penulis>> dataPenulis = penulisRepository.search(keyword);
        Optional<List<Penerbit>> dataPenerbit = penerbitRepository.search(keyword);
        if (dataPenulis.isPresent() || dataPenerbit.isPresent() || dataBuku.isPresent()){
            if(dataBuku.isPresent()){
                for(Buku data: dataBuku.get()){
                    List<String> namaPenulis = getDataBukuPenulis(data.getIsbn(), data);
                    BukuResponse buku = new BukuResponse(
                            data.getIsbn(),
                            data.getJudulBuku(),
                            namaPenulis,
                            data.getCoverBuku(),
                            data.getStockBuku(),
                            data.getHargaBuku()
                    );
                    finalData.add(buku);
                }
            }
            if (dataPenulis.isPresent()){
                for (Penulis penulis: dataPenulis.get()){
                    Optional<List<ListPenulisBuku>> data =
                            listPenulisBukuRepository.findByIdPenulis(penulis.getIdPenulis());
                    for (ListPenulisBuku item:data.get()){
                        if (!finalData.isEmpty()){
                            for (BukuResponse response:finalData){
                                if (item.getIsbn()!=response.getIsbn()){
                                    Optional<Buku> buku = bukuRepository.findById(item.getIsbn());
                                    if (buku.isPresent()){
                                        List<String> namaPenulis = getDataBukuPenulis(buku.get().getIsbn(), buku.get());
                                        BukuResponse result = new BukuResponse(
                                                buku.get().getIsbn(),
                                                buku.get().getJudulBuku(),
                                                namaPenulis,
                                                buku.get().getCoverBuku(),
                                                buku.get().getStockBuku(),
                                                buku.get().getHargaBuku()
                                        );
                                        finalData.add(result);
                                    }
                                    else{
                                        break;
                                    }
                                }
                            }
                        }
                        else{
                            Optional<Buku> buku = bukuRepository.findById(item.getIsbn());
                            if (buku.isPresent()){
                                List<String> namaPenulis = getDataBukuPenulis(buku.get().getIsbn(), buku.get());
                                BukuResponse result = new BukuResponse(
                                        buku.get().getIsbn(),
                                        buku.get().getJudulBuku(),
                                        namaPenulis,
                                        buku.get().getCoverBuku(),
                                        buku.get().getStockBuku(),
                                        buku.get().getHargaBuku()
                                );
                                finalData.add(result);
                            }
                        }
                    }
                }
            }
            if (dataPenerbit.isPresent()){
                for(Penerbit penerbit: dataPenerbit.get()){
                    Optional<List<Buku>> bukus = bukuRepository.findByPenerbit(penerbit.getNamaPenerbit());
                    if (bukus.isPresent()){
                        for(Buku data:bukus.get()){
                            if (!finalData.isEmpty()){
                                for(BukuResponse item:finalData){
                                    if (data.getIsbn() != item.getIsbn()){
                                        List<String> namaPenulis = getDataBukuPenulis(data.getIsbn(), data);
                                        BukuResponse buku = new BukuResponse(
                                                data.getIsbn(),
                                                data.getJudulBuku(),
                                                namaPenulis,
                                                data.getCoverBuku(),
                                                data.getStockBuku(),
                                                data.getHargaBuku()
                                        );
                                        finalData.add(buku);
                                    }
                                    else{
                                        break;
                                    }
                                }
                            }
                            else{
                                List<String> namaPenulis = getDataBukuPenulis(data.getIsbn(), data);
                                BukuResponse buku = new BukuResponse(
                                        data.getIsbn(),
                                        data.getJudulBuku(),
                                        namaPenulis,
                                        data.getCoverBuku(),
                                        data.getStockBuku(),
                                        data.getHargaBuku()
                                );
                                finalData.add(buku);
                            }
                        }
                    }
                }
            }
            return finalData;
        }
        throw new NotFoundException();
    }

    private List<String> getDataBukuPenulis(String isbn, Buku data) {
        Optional<List<ListPenulisBuku>> listPenulis =
                listPenulisBukuRepository.findByIsbn(isbn);
        List<String>namaPenulis = new ArrayList<>();
        for(ListPenulisBuku relation: listPenulis.get()){
            Optional<Penulis> penulis = penulisRepository.findById(relation.getIdPenulis());
            if(penulis.isPresent())
                namaPenulis.add(penulis.get().getNamaPenulis());
        }
        return namaPenulis;
    }

    public List<BukuResponse> getSameGenreByIsbn(String isbn){
        Optional<Buku> findDataIsbn = bukuRepository.findById(isbn);
        if (findDataIsbn.isPresent()){
            Optional<List<ListGenreBuku>> listGenreBuku =
                    listGenreBukuRepository.findByIsbn(isbn);
            return getBukuResponses(listGenreBuku.get());
        }
        throw new NotFoundException();
    }

}