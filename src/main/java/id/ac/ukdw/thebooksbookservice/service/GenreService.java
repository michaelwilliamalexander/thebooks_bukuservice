package id.ac.ukdw.thebooksbookservice.service;

import id.ac.ukdw.thebooksbookservice.dto.GenreDTO;
import id.ac.ukdw.thebooksbookservice.exception.BadRequestException;
import id.ac.ukdw.thebooksbookservice.exception.NotFoundException;
import id.ac.ukdw.thebooksbookservice.model.Genre;
import id.ac.ukdw.thebooksbookservice.repository.dao.GenreDao;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.valid4j.Validation.validate;

@Service
@RequiredArgsConstructor
public class GenreService {

    private final ModelMapper mapper;

    private final GenreDao genreRepository;

    public String addGenre(String idGenre, String namaGenre){
        validate(!idGenre.isEmpty(), new BadRequestException());
        validate(idGenre !=null, new BadRequestException());
        validate(!namaGenre.isEmpty(), new BadRequestException());
        validate(namaGenre!=null, new BadRequestException());

        Optional<Genre> findGenre = genreRepository.findById(idGenre);
        Optional<Genre> findNamaGenre = genreRepository.findByNamaGenre(namaGenre);
        if(findGenre.isEmpty() && findNamaGenre.isEmpty()){
            Genre genre = new Genre(idGenre, namaGenre);
            if(genreRepository.save(genre)) return "Penyimpanan Berhasil";
        }
        throw new BadRequestException();
    }

    public String updateGenre(String idGenre, String namaGenre){
        validate(!idGenre.isEmpty(), new BadRequestException());
        validate(idGenre !=null, new BadRequestException());
        validate(!namaGenre.isEmpty(), new BadRequestException());
        validate(namaGenre!=null, new BadRequestException());

        Optional<Genre> findGenre = genreRepository.findById(idGenre);
        Optional<Genre> findNamaGenre = genreRepository.findByNamaGenre(namaGenre);
        if (findGenre.isPresent() && findNamaGenre.isEmpty()){
            Genre genre = new Genre(idGenre,namaGenre);;
            if (genreRepository.update(genre)) return "Update Berhasil";
        }
        throw new BadRequestException();
    }

    public List<GenreDTO> getAllGenre(){
        Optional<List<Genre>> findData = genreRepository.getAll();
        if (findData.isPresent()){
            List<GenreDTO> result = new ArrayList<>();
            for(Genre genre:findData.get()){
                result.add(mapper.map(genre,GenreDTO.class));
            }
            return result;
        }
        throw new NotFoundException();
    }

    public String deleteGenre(String idGenre){
        Optional<Genre> genre = genreRepository.findById(idGenre);
        if(genre.isPresent()){
            if(genreRepository.delete(idGenre)) return "Delete Berhasil";
        }
        throw new BadRequestException();
    }



}
