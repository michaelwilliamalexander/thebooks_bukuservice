package id.ac.ukdw.thebooksbookservice.service;

import id.ac.ukdw.thebooksbookservice.dto.PenulisDTO;
import id.ac.ukdw.thebooksbookservice.exception.BadRequestException;
import id.ac.ukdw.thebooksbookservice.exception.NotFoundException;
import id.ac.ukdw.thebooksbookservice.model.Penulis;
import id.ac.ukdw.thebooksbookservice.repository.dao.PenulisDao;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.valid4j.Validation.validate;

@Service
@RequiredArgsConstructor
public class PenulisService {

    private final ModelMapper mapper;

    private final PenulisDao penulisRepository;

    public String addPenulis(String idPenulis, String namaPenulis){
        validate(!idPenulis.isEmpty(), new BadRequestException());
        validate(idPenulis !=null, new BadRequestException());
        validate(!namaPenulis.isEmpty(), new BadRequestException());
        validate(namaPenulis!=null, new BadRequestException());

        Optional<Penulis> findPenulis = penulisRepository.findById(idPenulis);
        Optional<Penulis> findNamaPenulis = penulisRepository.findByNamaPenulis(namaPenulis);
        if(findPenulis.isEmpty()&& findNamaPenulis.isEmpty()){
            Penulis penulis = new Penulis(idPenulis, namaPenulis);
            if(penulisRepository.save(penulis)) return "Penyimpanan Berhasil";
        }
        throw new BadRequestException();
    }

    public String updatePenulis(String idPenulis, String namaPenulis){
        validate(!idPenulis.isEmpty(), new BadRequestException());
        validate(idPenulis !=null, new BadRequestException());
        validate(!namaPenulis.isEmpty(), new BadRequestException());
        validate(namaPenulis!=null, new BadRequestException());

        Optional<Penulis> findPenulis = penulisRepository.findById(idPenulis);
        Optional<Penulis> findNamaPenulis = penulisRepository.findByNamaPenulis(namaPenulis);
        if (findPenulis.isPresent() && findNamaPenulis.isEmpty()){
            Penulis penulis = new Penulis(idPenulis,namaPenulis);
            if (penulisRepository.update(penulis)) {
                return "Update Berhasil";
            }
        }
        throw new BadRequestException();
    }

    public List<PenulisDTO> getAllPenulis(){
        Optional<List<Penulis>> findData = penulisRepository.getAll();
        if (findData.isPresent()){
            List<PenulisDTO> result = new ArrayList<>();
            for(Penulis penulis:findData.get()){
                result.add(mapper.map(penulis,PenulisDTO.class));
            }
            return result;
        }
        throw new NotFoundException();
    }

    public String deletePenulis(String idPenulis){
        Optional<Penulis> penulis = penulisRepository.findById(idPenulis);
        if(penulis.isPresent()){
            if(penulisRepository.delete(idPenulis)) return "Delete Berhasil";
        }
        throw new BadRequestException();
    }

}
