package id.ac.ukdw.thebooksbookservice.repository;

import id.ac.ukdw.thebooksbookservice.model.Buku;
import id.ac.ukdw.thebooksbookservice.model.Genre;
import id.ac.ukdw.thebooksbookservice.repository.dao.GenreDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Transactional
@Repository
@RequiredArgsConstructor
public class GenreRepository implements GenreDao {
    
    private final EntityManager entityManager;
    
    private final Logger log = Logger.getLogger(GenreRepository.class.getSimpleName());

    @Override
    public Optional<Genre> findById(String id) {
        String hql = "select gnr from Genre gnr where gnr.idGenre = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id",id);
        List<Genre> data = query.getResultList();
        Genre result = null;
        for (Genre genre:data){
            result = genre;
        }
        return Optional.ofNullable(result);
    }

    @Override
    public Optional<Genre> findByNamaGenre(String namaGenre) {
        String hql = "select gnr from Genre gnr where gnr.namaGenre = :namaGenre";
        Query query = entityManager.createQuery(hql);
        query.setParameter("namaGenre",namaGenre);
        List<Genre> data = query.getResultList();
        Genre result = null;
        for (Genre genre:data){
            result = genre;
        }
        return Optional.ofNullable(result);
    }

    @Override
    public Optional<List<Genre>> getAll() {
        String hql = "select gnr from Genre gnr";
        Query query = entityManager.createQuery(hql);
        log.info(query.toString());
        List<Genre> genreList = query.getResultList();
        return Optional.ofNullable(genreList);
    }

    @Override
    public boolean save(Genre genre) {
        String hql = "insert into genre(id_genre, nama_genre) values(?,?)";
        Query query = entityManager.createNativeQuery(hql);
        query.setParameter(1, genre.getIdGenre());
        query.setParameter(2, genre.getNamaGenre());
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean delete(String s) {
        String hql = "delete from Genre gnr where gnr.idGenre = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", s);
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean update(Genre genre) {
        entityManager.merge(genre);
        return true;
    }
}
