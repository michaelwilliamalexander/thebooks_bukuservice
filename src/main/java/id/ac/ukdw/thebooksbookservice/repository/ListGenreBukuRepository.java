package id.ac.ukdw.thebooksbookservice.repository;

import id.ac.ukdw.thebooksbookservice.model.ListGenreBuku;
import id.ac.ukdw.thebooksbookservice.repository.dao.ListGenreBukuDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Repository
@Transactional
@RequiredArgsConstructor
public class ListGenreBukuRepository implements ListGenreBukuDao {
    
    private final EntityManager entityManager;
        
    @Override
    public Optional<ListGenreBuku> findById(Integer id) {
        String hql = "select lgn from ListGenreBuku lgn where lgn.id = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id",id);
        List<ListGenreBuku> listGenreBukuList = query.getResultList();
        ListGenreBuku result = null;
        for (ListGenreBuku data: listGenreBukuList){
            result = data;
        }
        return Optional.ofNullable(result);
    }

    @Override
    public Optional<List<ListGenreBuku>> getAll() {
        String hql = "select listgnr from ListGenreBuku listgnr";
        Query query = entityManager.createQuery(hql);
        List<ListGenreBuku> listGenreBukuList = query.getResultList();
        return Optional.ofNullable(listGenreBukuList);
    }

    @Override
    public boolean save(ListGenreBuku listGenreBuku) {
        String hql = "insert into list_genre(id_genre,isbn) values(?,?)";
        Query query = entityManager.createNativeQuery(hql);
        query.setParameter(1, listGenreBuku.getIdGenre());
        query.setParameter(2, listGenreBuku.getIsbn());
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        String hql = "delete from ListGenreBuku listgnr where listgnr.id = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", id);
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean update(ListGenreBuku listGenreBuku) {
        entityManager.merge(listGenreBuku);
        return true;
    }

    @Override
    public Optional<ListGenreBuku> findByIdGenreAndIsbn(String idGenre, String isbn) {
        String hql = "select lgn from ListGenreBuku lgn where lgn.isbn = :isbn and lgn.idGenre = :idGenre";
        Query query = entityManager.createQuery(hql);
        query.setParameter("isbn",isbn);
        query.setParameter("idGenre",idGenre);
        List<ListGenreBuku> listGenreBukuList = query.getResultList();
        ListGenreBuku result = null;
        for (ListGenreBuku data: listGenreBukuList){
            result = data;
        }
        return Optional.ofNullable(result);
    }

    @Override
    public Optional<List<ListGenreBuku>> findByIsbn(String isbn) {
        String hql = "select listgnr from ListGenreBuku listgnr where listgnr.isbn = :isbn";
        Query query = entityManager.createQuery(hql);
        query.setParameter("isbn",isbn);
        List<ListGenreBuku> listGenreBukuList = query.getResultList();
        return Optional.ofNullable(listGenreBukuList);
    }

    @Override
    public Optional<List<ListGenreBuku>> findByIdGenre(String idGenre) {
        String hql = "select listgnr from ListGenreBuku listgnr where listgnr.idGenre = :idGenre";
        Query query = entityManager.createQuery(hql);
        query.setParameter("idGenre",idGenre);
        List<ListGenreBuku> listGenreBukuList = query.getResultList();
        return Optional.ofNullable(listGenreBukuList);
    }
}
