package id.ac.ukdw.thebooksbookservice.repository.dao;

import id.ac.ukdw.thebooksbookservice.model.Buku;

import java.util.List;
import java.util.Optional;

public interface BukuDao extends Dao<Buku, String>{

    boolean update(String id,Buku buku);

    Optional<List<Buku>> searchBuku(String keyword);

    Optional<List<Buku>> findByPenerbit(String penerbit);

}
