package id.ac.ukdw.thebooksbookservice.repository;

import id.ac.ukdw.thebooksbookservice.model.Buku;
import id.ac.ukdw.thebooksbookservice.repository.dao.BukuDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Repository
@RequiredArgsConstructor
public class BukuRepository implements BukuDao {

    private final EntityManager entityManager;

    @Override
    public Optional<Buku> findById(String isbn) {
        String hql = "select buku from Buku buku where buku.isbn = :isbn";
        Query query = entityManager.createQuery(hql);
        query.setParameter("isbn", isbn);
        List<Buku> result = query.getResultList();
        Buku buku = null;
        for (Buku item:result){
            buku = item;
        }
        return Optional.ofNullable(buku);
    }

    @Override
    public Optional<List<Buku>> getAll() {
        String hql = "select buku from Buku buku";
        Query query = entityManager.createQuery(hql);
        List<Buku> bukuList = query.getResultList();
        return Optional.ofNullable(bukuList);
    }

    @Override
    public boolean save(Buku buku) {
        String hql = "insert into buku(isbn,judul_buku," +
                "deskripsi_buku,stock_buku,harga_buku," +
                "cover_buku,sample_buku,id_penerbit,tahun_terbit,jumlah_halaman) values(?,?,?,?,?,?,?,?,?,?)";
        Query query = entityManager.createNativeQuery(hql);
        query.setParameter(1,buku.getIsbn());
        query.setParameter(2,buku.getJudulBuku());
        query.setParameter(3,buku.getDeskripsiBuku());
        query.setParameter(4,buku.getStockBuku());
        query.setParameter(5,buku.getHargaBuku());
        query.setParameter(6,buku.getCoverBuku());
        query.setParameter(7,buku.getSampleBuku());
        query.setParameter(8,buku.getPenerbit());
        query.setParameter(9,buku.getTahunTerbit());
        query.setParameter(10,buku.getJumlahHalaman());
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean delete(String s) {
        String hql = "delete from Buku buku where buku.isbn = :isbn";
        Query query = entityManager.createQuery(hql);
        query.setParameter("isbn", s);
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean update(Buku buku) {
        entityManager.merge(buku);
        return true;
    }

    @Override
    public boolean update(String id, Buku buku) {
        String hql = "update Buku buku set buku.isbn = :isbn, buku.judulBuku = :judul, " +
                "buku.tahunTerbit = :terbit, buku.jumlahHalaman = :halaman, " +
                "buku.hargaBuku = :harga, buku.stockBuku = :stock, " +
                "buku.deskripsiBuku = :deskripsi, buku.penerbit = :penerbit, " +
                "buku.coverBuku = :cover, buku.sampleBuku = :sample where buku.isbn = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id",id);
        query.setParameter("isbn",buku.getIsbn());
        query.setParameter("judul",buku.getJudulBuku());
        query.setParameter("halaman",buku.getJumlahHalaman());
        query.setParameter("deskripsi",buku.getDeskripsiBuku());
        query.setParameter("penerbit", buku.getPenerbit());
        query.setParameter("cover", buku.getCoverBuku());
        query.setParameter("sample", buku.getSampleBuku());
        query.setParameter("harga", buku.getHargaBuku());
        query.setParameter("stock", buku.getStockBuku());
        query.setParameter("terbit",buku.getTahunTerbit());
        return true;
    }

    @Override
    public Optional<List<Buku>> searchBuku(String keyword) {
        String hql = "select buku from Buku buku where buku.judulBuku like :keyword";
        Query query = entityManager.createQuery(hql);
        query.setParameter("keyword","%"+keyword+"%");
        List<Buku> bukuList = query.getResultList();
        return Optional.ofNullable(bukuList);
    }

    @Override
    public Optional<List<Buku>> findByPenerbit(String penerbit) {
        String hql = "select buku from Buku buku where buku.penerbit.namaPenerbit = :penerbit";
        Query query = entityManager.createQuery(hql);
        query.setParameter("penerbit",penerbit);
        List<Buku> bukuList = query.getResultList();
        return Optional.ofNullable(bukuList);
    }
}
