package id.ac.ukdw.thebooksbookservice.repository.dao;

import id.ac.ukdw.thebooksbookservice.model.Genre;

import java.util.List;
import java.util.Optional;

public interface GenreDao extends Dao<Genre,String>{
    
    Optional<Genre> findByNamaGenre(String genre);
}
