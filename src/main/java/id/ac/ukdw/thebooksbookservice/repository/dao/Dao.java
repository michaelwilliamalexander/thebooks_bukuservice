package id.ac.ukdw.thebooksbookservice.repository.dao;

import java.util.List;
import java.util.Optional;

public interface Dao<T,ID> {

    Optional<T> findById(ID id);

    Optional<List<T>> getAll();
    
    boolean save(T t);

    boolean delete(ID id);

    boolean update(T t);
}