package id.ac.ukdw.thebooksbookservice.repository.dao;

import id.ac.ukdw.thebooksbookservice.model.Penulis;

import java.util.List;
import java.util.Optional;

public interface PenulisDao extends Dao<Penulis, String> {

    Optional<Penulis> findByNamaPenulis(String penulis);

    Optional<List<Penulis>> search(String keyword);
}
