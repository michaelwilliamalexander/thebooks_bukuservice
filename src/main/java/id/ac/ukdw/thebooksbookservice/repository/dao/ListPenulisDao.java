package id.ac.ukdw.thebooksbookservice.repository.dao;

import id.ac.ukdw.thebooksbookservice.model.ListPenulisBuku;

import java.util.List;
import java.util.Optional;

public interface ListPenulisDao extends Dao<ListPenulisBuku,Integer>{

    Optional<ListPenulisBuku> findByIdPenulisAndIsbn(String idPenulis, String isbn);

    Optional<List<ListPenulisBuku>> findByIsbn(String isbn);

    Optional<List<ListPenulisBuku>> findByIdPenulis(String idPenulis);
}
