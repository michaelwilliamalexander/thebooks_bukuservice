package id.ac.ukdw.thebooksbookservice.repository.dao;

import id.ac.ukdw.thebooksbookservice.model.ListGenreBuku;

import java.util.List;
import java.util.Optional;

public interface ListGenreBukuDao extends Dao<ListGenreBuku,Integer>{

    Optional<ListGenreBuku> findByIdGenreAndIsbn(String idGenre,String isbn);

    Optional<List<ListGenreBuku>> findByIsbn(String isbn);

    Optional<List<ListGenreBuku>> findByIdGenre(String idGenre);
}
