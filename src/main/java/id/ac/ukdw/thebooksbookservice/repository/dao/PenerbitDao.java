package id.ac.ukdw.thebooksbookservice.repository.dao;

import id.ac.ukdw.thebooksbookservice.model.Penerbit;

import java.util.List;
import java.util.Optional;

public interface PenerbitDao extends Dao<Penerbit,String> {

    Optional<Penerbit> findByNamaPenerbit(String penulis);

    Optional<List<Penerbit>> search(String keyword);
}
