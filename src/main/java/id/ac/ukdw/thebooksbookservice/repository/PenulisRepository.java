package id.ac.ukdw.thebooksbookservice.repository;

import id.ac.ukdw.thebooksbookservice.model.Penulis;
import id.ac.ukdw.thebooksbookservice.repository.dao.PenulisDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import javax.transaction.Transactional;

@Transactional
@Repository
@RequiredArgsConstructor
public class PenulisRepository implements PenulisDao{

    private final EntityManager entityManager;

    private final Logger log = Logger.getLogger(PenulisRepository.class.getSimpleName());

    @Override
    public Optional<Penulis> findById(String id){
        String hql = "select pnl from Penulis pnl where pnl.idPenulis = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", id);
        List<Penulis> penulisList = query.getResultList();
        Penulis penulis = null;
        for(Penulis p:penulisList){
            penulis = p;
        }
        return Optional.ofNullable(penulis);

    }

    @Override
    public Optional<Penulis> findByNamaPenulis(String namaPenulis) {
        String hql = "select pnl from Penulis pnl where pnl.idPenulis = :namaPenulis";
        Query query = entityManager.createQuery(hql);
        query.setParameter("namaPenulis", namaPenulis);
        List<Penulis> penulisList = query.getResultList();
        Penulis penulis = null;
        for(Penulis p:penulisList){
            penulis = p;
        }
        return Optional.ofNullable(penulis);
    }

    @Override
    public Optional<List<Penulis>> search(String keyword) {
        String hql = "select pnl FROM Penulis pnl where pnl.namaPenulis like :keyword";
        Query query = entityManager.createQuery(hql);
        query.setParameter("keyword","%"+keyword+"%");
        List<Penulis> penulisList = query.getResultList();
        return Optional.ofNullable(penulisList);
    }

    @Override
    public Optional<List<Penulis>> getAll() {
        String hql = "select pnl FROM Penulis pnl";
        Query query = entityManager.createQuery(hql);
        List<Penulis> penulisList = query.getResultList();
        return Optional.ofNullable(penulisList);
    }

    @Override
    public boolean save(Penulis penulis) {
        String hql = "insert into penulis(id_penulis,nama_penulis) values (?,?)";
        Query query = entityManager.createNativeQuery(hql);
        query.setParameter(1,penulis.getIdPenulis());
        query.setParameter(2,penulis.getNamaPenulis());
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean delete(String id) {
        String hql = "delete from Penulis pnl where pnl.idPenulis = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id",id);
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean update(Penulis penulis) {
        entityManager.merge(penulis);
        return true;
    }


}
