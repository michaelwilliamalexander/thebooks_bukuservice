package id.ac.ukdw.thebooksbookservice.repository;

import id.ac.ukdw.thebooksbookservice.model.Penerbit;
import id.ac.ukdw.thebooksbookservice.repository.dao.PenerbitDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Transactional
@Repository
@RequiredArgsConstructor
public class PenerbitRepository implements PenerbitDao {

    private final EntityManager entityManager;

    private final Logger log = Logger.getLogger(PenerbitRepository.class.getSimpleName());


    @Override
    public Optional<Penerbit> findById(String id){
        String hql = "select pnr from Penerbit pnr where pnr.idPenerbit = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", id);
        List<Penerbit> penerbitList = query.getResultList();
        Penerbit penerbit = null;
        for(Penerbit p:penerbitList){
            penerbit = p;
        }
        return Optional.ofNullable(penerbit);

    }

    @Override
    public Optional<List<Penerbit>> getAll() {
        String hql = "select pnl FROM Penerbit pnl";
        Query query = entityManager.createQuery(hql);
        log.info(query.toString());
        List<Penerbit> penerbitList = query.getResultList();
        return Optional.ofNullable(penerbitList);
    }

    @Override
    public boolean save(Penerbit penerbit) {
        String hql = "insert into penerbit(id_penerbit,nama_penerbit) values (?,?)";
        Query query = entityManager.createNativeQuery(hql);
        query.setParameter(1,penerbit.getIdPenerbit().toUpperCase());
        query.setParameter(2,penerbit.getNamaPenerbit());
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean delete(String id) {
        String hql = "delete from Penerbit pnl where pnl.idPenerbit = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id",id);
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean update(Penerbit penerbit) {
        entityManager.merge(penerbit);
        return true;
    }

    @Override
    public Optional<Penerbit> findByNamaPenerbit(String penerbit) {
        String hql = "select pnr from Penerbit pnr where pnr.namaPenerbit= :nama";
        Query query = entityManager.createQuery(hql);
        query.setParameter("nama",penerbit);
        List<Penerbit> dataResult = query.getResultList();
        Penerbit result = null;
        for(Penerbit p: dataResult){
            result = p;
        }
        return Optional.ofNullable(result);

    }

    @Override
    public Optional<List<Penerbit>> search(String keyword) {
        String hql = "select pnl FROM Penerbit pnl where pnl.namaPenerbit like :keyword";
        Query query = entityManager.createQuery(hql);
        query.setParameter("keyword","%"+keyword+"%");
        List<Penerbit> penerbitList = query.getResultList();
        return Optional.ofNullable(penerbitList);
    }
}
