package id.ac.ukdw.thebooksbookservice.repository;

import id.ac.ukdw.thebooksbookservice.model.ListPenulisBuku;
import id.ac.ukdw.thebooksbookservice.repository.dao.ListPenulisDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Repository
@Transactional
@RequiredArgsConstructor
public class ListPenulisBukuRepository implements ListPenulisDao {

    private final EntityManager entityManager;

    private final Logger log = Logger.getLogger(ListPenulisBukuRepository.class.getSimpleName());

    @Override
    public Optional<ListPenulisBuku> findById(Integer id) {
        String hql = "select lpn from ListPenulisBuku lpn where  lpn.id = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id",id);
        List<ListPenulisBuku> listPenulisBukulist = query.getResultList();
        ListPenulisBuku result = null;
        for (ListPenulisBuku item: listPenulisBukulist){
            result = item;
        }
        return Optional.ofNullable(result);
    }

    @Override
    public Optional<List<ListPenulisBuku>> getAll() {
        String hql = "select listpenulis from ListPenulisBuku listpenulis";
        Query query = entityManager.createQuery(hql);
        List<ListPenulisBuku> listPenulisBukulist = query.getResultList();
        return Optional.ofNullable(listPenulisBukulist);
    }

    @Override
    public boolean save(ListPenulisBuku listPenulisBuku) {
        String hql = "insert into list_penulis(isbn,id_penulis) values(?,?)";
        Query query = entityManager.createNativeQuery(hql);
        query.setParameter(1, listPenulisBuku.getIsbn());
        query.setParameter(2, listPenulisBuku.getIdPenulis());
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        String hql = "delete from ListPenulisBuku listpenulis where listpenulis.id = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", id);
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean update(ListPenulisBuku listPenulisBuku) {
        entityManager.merge(listPenulisBuku);
        return true;
    }

    @Override
    public Optional<ListPenulisBuku> findByIdPenulisAndIsbn(String idPenulis, String isbn) {
        String hql = "select lpn from ListPenulisBuku lpn where  lpn.idPenulis = :idPenulis and lpn.isbn = :isbn";
        Query query = entityManager.createQuery(hql);
        query.setParameter("idPenulis",idPenulis);
        query.setParameter("isbn",isbn);
        List<ListPenulisBuku> listPenulisBukulist = query.getResultList();
        ListPenulisBuku result = null;
        for (ListPenulisBuku item: listPenulisBukulist){
            result = item;
        }
        return Optional.ofNullable(result);
    }

    @Override
    public Optional<List<ListPenulisBuku>> findByIsbn(String isbn) {
        String hql = "select listpnls from ListPenulisBuku listpnls where listpnls.isbn = :isbn";
        Query query = entityManager.createQuery(hql);
        query.setParameter("isbn",isbn);
        List<ListPenulisBuku> listPenulisBukuList = query.getResultList();
        return Optional.ofNullable(listPenulisBukuList);
    }

    @Override
    public Optional<List<ListPenulisBuku>> findByIdPenulis(String idPenulis) {
        String hql = "select listpnls from ListPenulisBuku listpnls " +
                "where listpnls.idPenulis = :idPenulis";
        Query query = entityManager.createQuery(hql);
        query.setParameter("idPenulis",idPenulis);
        List<ListPenulisBuku> listPenulisBukuList = query.getResultList();
        return Optional.ofNullable(listPenulisBukuList);
    }
}