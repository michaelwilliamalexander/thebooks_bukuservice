package id.ac.ukdw.thebooksbookservice.model;


import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

@Entity
@Data
@Table(name = "penerbit")
public class Penerbit {

    @Id
    @Column(name = "id_penerbit")
    private String idPenerbit;

    @Column(name = "nama_penerbit")
    private String namaPenerbit;
    
    public Penerbit(){}

    public Penerbit(String idPenerbit, String namaPenerbit){
        this.idPenerbit = idPenerbit;
        this.namaPenerbit = namaPenerbit;
    }
}
