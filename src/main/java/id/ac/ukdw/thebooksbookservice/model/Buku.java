package id.ac.ukdw.thebooksbookservice.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "buku")
public class Buku {

    @Id
    @Column(name = "isbn")
    private String isbn;

    @Column(name = "judul_buku", nullable = false)
    private String judulBuku;

    @Column(name = "stock_buku")
    private int stockBuku;

    @Column(name = "harga_buku")
    private double hargaBuku;

    @Column(name = "deskripsi_buku")
    private String deskripsiBuku;

    @Column(name = "jumlah_halaman")
    private int jumlahHalaman;

    @Column(name = "tahun_terbit")
    private String tahunTerbit;

    @Lob
    @Column(name = "cover_buku")
    private byte[] coverBuku;

    @Lob
    @Column(name = "sample_buku", columnDefinition = "longblob")
    private byte[] sampleBuku;

    @ManyToOne
    @JoinColumn(name = "id_penerbit", nullable = false)
    private Penerbit penerbit;
    
    public Buku(){}
    
    public Buku(
        String isbn,
        String judulBuku,
        String deskripsiBuku,
        int jumlahHalaman,
        String tahunTerbit,
        double hargaBuku,
        int stockBuku,
        byte[] coverBuku,
        byte[] sampleBuku,
        Penerbit penerbit
    ){
        this.isbn = isbn;
        this.judulBuku = judulBuku;
        this.deskripsiBuku = deskripsiBuku;
        this.jumlahHalaman = jumlahHalaman;
        this.tahunTerbit = tahunTerbit;
        this.hargaBuku = hargaBuku;
        this.stockBuku = stockBuku;
        this.coverBuku = coverBuku;
        this.sampleBuku = sampleBuku;
        this.penerbit = penerbit;
    }

}
