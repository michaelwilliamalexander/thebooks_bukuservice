package id.ac.ukdw.thebooksbookservice.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "list_penulis")
@Data
public class ListPenulisBuku {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    
    @Column(name = "isbn")
    private String isbn;
    
    @Column(name = "id_penulis")
    private String idPenulis;

    public ListPenulisBuku(){}

    public ListPenulisBuku(String idPenulis,String isbn){
        this.isbn = isbn;
        this.idPenulis = idPenulis;
    }
}
