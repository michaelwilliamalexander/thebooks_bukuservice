package id.ac.ukdw.thebooksbookservice.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "list_genre")
public class ListGenreBuku {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "id_genre")
    private String idGenre;

    @Column(name = "isbn")
    private String isbn;

    public ListGenreBuku(){}

    public ListGenreBuku(String idGenre, String isbn){
        this.idGenre = idGenre;
        this.isbn = isbn;
    }
}
