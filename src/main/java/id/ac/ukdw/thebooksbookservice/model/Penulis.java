package id.ac.ukdw.thebooksbookservice.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "penulis")
public class Penulis {

    @Id
    @Column(name = "id_penulis")
    private String idPenulis;

    @Column(name = "nama_penulis")
    private String namaPenulis;

    public Penulis(){}

    public Penulis(String idPenulis,String namaPenulis){
        this.idPenulis = idPenulis;
        this.namaPenulis = namaPenulis;
    }

}
