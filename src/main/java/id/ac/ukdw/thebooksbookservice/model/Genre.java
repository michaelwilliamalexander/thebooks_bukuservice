package id.ac.ukdw.thebooksbookservice.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "genre")
public class Genre {

    @Id
    @Column(name = "id_genre")
    private String idGenre;

    @Column(name = "nama_genre")
    private String namaGenre;

    public Genre(){}

    public Genre(String idGenre,String namaGenre){
        this.idGenre =idGenre;
        this.namaGenre = namaGenre;
    }
}
