package id.ac.ukdw.thebooksbookservice.controller;

import id.ac.ukdw.thebooksbookservice.dto.request.BukuRequest;
import id.ac.ukdw.thebooksbookservice.dto.request.UpdateBukuRequest;
import id.ac.ukdw.thebooksbookservice.dto.response.ResponseWrapper;
import id.ac.ukdw.thebooksbookservice.service.BukuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.logging.Logger;

import static id.ac.ukdw.thebooksbookservice.config.SwaggerConfig.*;

@Controller
@RequestMapping("/buku")
@Api(tags = bukuService)
@RequiredArgsConstructor
public class BukuController {

    private final BukuService bukuService;

    private final Logger log = Logger.getLogger(BukuController.class.getSimpleName());

    @ApiOperation(value = "Mendapatkan data buku dengan genre serupa")
    @GetMapping(value = "/{isbn}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper> getBukuSameGenre(@PathVariable("isbn") String isbn){
        return ResponseEntity.ok(new ResponseWrapper(bukuService.getSameGenreByIsbn(isbn)));
    }

    @ApiOperation(value = "Mendapatkan data buku dengan spesifik genre")
    @GetMapping(value = "/genre/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper> getBukuByGenre(@PathVariable("id") String idGenre){
        return ResponseEntity.ok(new ResponseWrapper(bukuService.findBukuByIdGenre(idGenre)));
    }

    @ApiOperation(value = "Mendapatkan semua data buku")
    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper> getAllBuku(){
        return ResponseEntity.ok(new ResponseWrapper(bukuService.getAllBuku()));
    }

    @ApiOperation(value = "Mendapatkan detail data buku")
    @GetMapping(value = "/detail",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper> getDetailBuku(@RequestParam("isbn") String isbn){
        return ResponseEntity.ok(new ResponseWrapper(bukuService.getDetailBuku(isbn)));
    }

    @ApiOperation(value = "Mencari buku berdasarkan keyword")
    @GetMapping(value = "/search",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper> searchBuku(@RequestParam("keyword") String keyword){
        return ResponseEntity.ok(new ResponseWrapper(bukuService.searchBuku(keyword)));
    }

    @SneakyThrows
    @ApiOperation(value = "Menyimpan data buku")
    @PostMapping(value="/", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ResponseWrapper> saveBuku(
            @RequestPart("dataBuku") BukuRequest request,
            @RequestPart("coverBuku") MultipartFile coverBuku,
            @RequestPart("sampleBuku") MultipartFile sampleBuku){
        return ResponseEntity.ok(new ResponseWrapper(
            bukuService.saveBuku(
                request.getIsbn(),
                request.getJudulBuku(),
                request.getDeskripsiBuku(),
                request.getHargaBuku(),
                request.getStockBuku(),
                request.getJumlahHalaman(),
                request.getTahunTerbit(),
                request.getIdGenre(),
                request.getIdPenulis(),
                request.getIdPenerbit(),
                coverBuku.getBytes(),
                sampleBuku.getBytes()
            )
        ));
    }

    @ApiOperation(value = "Menghapus data buku")
    @DeleteMapping(value = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper> deletelBuku(@PathVariable("id") String id){
        return ResponseEntity.ok(new ResponseWrapper(bukuService.deleteBuku(id)));
    }

    @SneakyThrows
    @ApiOperation(value = "Memperbarui data buku")
    @PostMapping(value = "/{id}",consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ResponseWrapper> updateBuku(
            @PathVariable("id") String id,
            @RequestPart("dataBuku") UpdateBukuRequest request,
            @RequestPart("coverBuku") MultipartFile coverBuku,
            @RequestPart("sampleBuku") MultipartFile sampleBuku
    ){
        return ResponseEntity.ok(new ResponseWrapper(
                bukuService.updateBuku(
                        id,
                        request.getJudulBuku(),
                        request.getDeskripsiBuku(),
                        request.getHargaBuku(),
                        request.getStockBuku(),
                        request.getJumlahHalaman(),
                        request.getTahunTerbit(),
                        request.getIdGenre(),
                        request.getIdPenulis(),
                        request.getIdPenerbit(),
                        coverBuku.getBytes(),
                        sampleBuku.getBytes()
                )
        ));
    }
}