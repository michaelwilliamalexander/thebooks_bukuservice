package id.ac.ukdw.thebooksbookservice.controller;

import id.ac.ukdw.thebooksbookservice.dto.request.PenulisRequest;
import id.ac.ukdw.thebooksbookservice.dto.request.UpdatePenulisRequest;
import id.ac.ukdw.thebooksbookservice.dto.response.ResponseWrapper;
import id.ac.ukdw.thebooksbookservice.service.PenulisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import static id.ac.ukdw.thebooksbookservice.config.SwaggerConfig.*;

@Controller
@RequestMapping("/buku/penulis")
@Api(tags = penulisService)
@RequiredArgsConstructor
public class PenulisController {

    private final PenulisService service;

    @ApiOperation(value = "Mendapatkan semua data penulis")
    @GetMapping(value = "/",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper> getAllPenulis(){
        return ResponseEntity.ok(
                new ResponseWrapper(service.getAllPenulis()));
    }

    @ApiOperation(value = "Menambakan data penulis")
    @PostMapping(value = "/",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper> addPenulis(
            @RequestBody PenulisRequest request){
        return ResponseEntity.ok(new ResponseWrapper(
                service.addPenulis(request.getIdPenulis(),request.getNamaPenulis())));
    }

    @ApiOperation(value = "Memperbarui data penulis")
    @PutMapping(value = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper> updatePenulis(
            @PathVariable("id")String id,
            @RequestBody UpdatePenulisRequest request){
        return ResponseEntity.ok(new ResponseWrapper(
                service.updatePenulis(id,request.getNamaPenulis())
        ));
    }

    @ApiOperation(value = "Menghapus data penulis")
    @DeleteMapping(value = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper> deletePenulis(@PathVariable("id")String id){
        return ResponseEntity.ok(new ResponseWrapper(
            service.deletePenulis(id)
        ));
    }

}
