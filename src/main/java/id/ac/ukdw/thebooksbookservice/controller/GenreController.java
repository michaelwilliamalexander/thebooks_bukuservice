package id.ac.ukdw.thebooksbookservice.controller;

import id.ac.ukdw.thebooksbookservice.config.SwaggerConfig;
import id.ac.ukdw.thebooksbookservice.dto.request.GenreRequest;
import id.ac.ukdw.thebooksbookservice.dto.request.UpdateGenreRequest;
import id.ac.ukdw.thebooksbookservice.dto.response.ResponseWrapper;
import id.ac.ukdw.thebooksbookservice.service.GenreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import static id.ac.ukdw.thebooksbookservice.config.SwaggerConfig.*;

@Controller
@RequestMapping("/buku/genre")
@Api(tags = genreService)
@RequiredArgsConstructor
public class GenreController {

    private final GenreService service;

    @ApiOperation(value = "Mendapatkan semua data genre")
    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper> getAllGenre(){
        return ResponseEntity.ok(new ResponseWrapper(service.getAllGenre()));
    }

    @ApiOperation(value = "Menambahkan data genre")
    @PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper> addGenre(
            @RequestBody GenreRequest request){
        return ResponseEntity.ok(new ResponseWrapper(
                service.addGenre(request.getIdGenre(),
                        request.getNamaGenre())));
    }

    @ApiOperation(value = "Memperbarui data genre")
    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper> updateGenre(
            @PathVariable("id")String id,
            @RequestBody UpdateGenreRequest request){
        return ResponseEntity.ok(new ResponseWrapper(
                service.updateGenre(id,request.getNamaGenre())));
    }

    @ApiOperation(value = "Menghapus data genre")
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper> deleteGenre(@PathVariable("id")String id){
        return ResponseEntity.ok(new ResponseWrapper(service.deleteGenre(id)));
    }
}
