package id.ac.ukdw.thebooksbookservice.controller;

import id.ac.ukdw.thebooksbookservice.dto.request.PenerbitRequest;
import id.ac.ukdw.thebooksbookservice.dto.request.UpdatePenerbitRequest;
import id.ac.ukdw.thebooksbookservice.dto.response.ResponseWrapper;
import id.ac.ukdw.thebooksbookservice.service.PenerbitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import static id.ac.ukdw.thebooksbookservice.config.SwaggerConfig.*;

@Controller
@RequestMapping("/buku/penerbit")
@Api(tags = penerbitService)
@RequiredArgsConstructor
public class PenerbitController {

    private final PenerbitService service;

    @ApiOperation(value = "Mendapatkan semua data penerbit")
    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper> getAllPenerbit(){
        return ResponseEntity.ok(new ResponseWrapper(service.getAllPenerbit()));
    }

    @ApiOperation(value = "Menambahkan data penerbit")
    @PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper> addPenerbit(
            @RequestBody PenerbitRequest request){
        return ResponseEntity.ok(new ResponseWrapper(
            service.addPenerbit(request.getIdPenerbit(),
                                request.getNamaPenerbit())));
    }

    @ApiOperation(value = "Memperbarui data penerbit")
    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper> updatePenerbit(
            @PathVariable("id") String id,
            @RequestBody UpdatePenerbitRequest request){
        
        return ResponseEntity.ok(new ResponseWrapper(
            service.updatePenerbit(id,request.getNamaPenerbit())
        ));
    }

    @ApiOperation(value = "Menghapus data penerbit")
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper> deletePenerbit(@PathVariable("id")String id){
        return ResponseEntity.ok(new ResponseWrapper(service.deletePenerbit(id)));
    }
    
}
