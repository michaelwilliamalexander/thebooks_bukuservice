Feature: Memperbarui Buku

  Scenario Outline: Memperbarui Data Buku dan Berhasil
    Given List data Buku
      |isbn           |judulBuku                                                      |namaPengarang                |coverBuku               |sampleBuku               |namaPenerbit             |deskripsiBuku            |namaGenre           |jumlahHalaman |tahunTerbit |stockBuku  |hargaBuku  |
      |9781338132311  |Fantastic Beasts and Where to Find Them                        |J.K. Rowling, Newt Scamander |image.png               |image.png                |Arthur A. Levine Books   |Buku Fantasi Beast       |Fantasy             |128           |2017        |200        |110000     |
      |9781974710041  |Jujutsu Kaisen Volume 3                                        |Gege Akutami                 |image.png               |image.png                |Elex Media Komputindo    |Lanjutan dari volume 2   |Adventure, Fantasy  |192           |2020        |50         |100000     |
      |9780804171588  |Crazy Rich Asians                                              |Kevin Kwan                   |image.png               |image.png                |Sinar Star Book          |                         |Novel               |480           |2014        |100        |115000     |
    When user memperbarui buku berid <isbn> dengan data update <judulBuku> , <namaPengarang> , <coverBuku> , <deskripsiBuku> , <sampleBuku> , <namaPenerbit> , <namaGenre> , <tahunTerbit> , <jumlahHalaman> , <hargaBuku> , <stockBuku>
    Then user memperoleh pesan <pesan> dari data update buku <isbn> , <judulBuku> , <namaPengarang> , <coverBuku> , <deskripsiBuku> , <sampleBuku> , <namaPenerbit> , <namaGenre> , <tahunTerbit> , <jumlahHalaman> , <hargaBuku> , <stockBuku>
    Examples:
      |isbn           |judulBuku                                |namaPengarang                 |coverBuku |sampleBuku |namaPenerbit             |deskripsiBuku            |namaGenre            |hargaBuku |jumlahHalaman |stockBuku    |tahunTerbit   |pesan                  |
      |9781338132311  |Fantastic Beasts and Where to Find Them  |J.K. Rowling, Newt Scamander  |image.png |image.png  |Arthur A. Levine Books   |Buku Fantasi Beast       |Fantasy              |150000    |99            |100          |2019          |update buku berhasil   |
      |9781974710041  |Jujutsu Kaisen Volume 3                  |Gege Akutami                  |image.png |image.png  |Elex Media Komputindo    |Lanjutan dari volume 2   |Adventure, Fantasy   |200000    |99            |50           |2020          |update buku berhasil   |

  Scenario Outline: Memperbarui Data Buku dan Gagal
    Given List data-data Buku
      |isbn           |judulBuku                               |namaPengarang                |coverBuku |sampleBuku  |namaPenerbit             |deskripsiBuku            |namaGenre          |jumlahHalaman |tahunTerbit |stockBuku  |hargaBuku  |
      |9781338132311  |Fantastic Beasts and Where to Find Them |J.K. Rowling, Newt Scamander |image.png |image.png   |Arthur A. Levine Books   |Buku Fantasi Beast       |Fantasy            |128           |2017        |200        |110000     |
      |9781974710041  |Jujutsu Kaisen Volume 3                 |Gege Akutami                 |image.png |image.png   |Elex Media Komputindo    |Lanjutan dari volume 2   |Adventure, Fantasy |192           |2020        |50         |100000     |
      |9780804171588  |Crazy Rich Asians                       |Kevin Kwan                   |image.png |image.png   |Sinar Star Book          |                         |Novel              |480           |2014        |100        |115000     |
    When user mengupdate buku berid <isbn> dengan data update <judulBuku> , <namaPengarang> , <coverBuku> , <deskripsiBuku> , <sampleBuku> , <namaPenerbit> , <namaGenre> , <tahunTerbit> , <jumlahHalaman> , <hargaBuku> , <stockBuku>
    Then user memperoleh pesan Bad Request dari data buku update buku <isbn> , <judulBuku> , <namaPengarang> , <coverBuku> , <deskripsiBuku> , <sampleBuku> , <namaPenerbit> , <namaGenre> , <tahunTerbit> , <jumlahHalaman> , <hargaBuku> , <stockBuku>
    Examples:
      |isbn           |judulBuku         |namaPengarang  |coverBuku   |sampleBuku  |namaPenerbit    |deskripsiBuku            |namaGenre              |jumlahHalaman |stockBuku    |tahunTerbit   |hargaBuku |
      |9780345803782  |Crazy Rich Asians |Kevin Kwan     |image.png   |image.png   |Sinar Star Book |                         |Novel                  |544           |200          |2019          |115000    |
      |9780345803788  |                  |Kevin Kwan     |            |            |Sinar Star Book |                         |Novel                  |544           |100          |              |115000    |
