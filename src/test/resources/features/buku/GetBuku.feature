Feature: Get Buku

  Scenario: Menampilkan Semua data Buku dan ditemukan
    Given Terdapat data-data buku
      |isbn           |judulBuku                                |namaPengarang                |coverBuku |stockBuku  |hargaBuku  |
      |9781338132311  |Fantastic Beasts and Where to Find Them  |J.K. Rowling, Newt Scamander |image.png |200        |110000     |
      |9781974710041  |Jujutsu Kaisen Volume 3                  |Gege Akutami                 |image.png |50         |100000     |
      |9780804171588  |Crazy Rich Asians                        |Kevin Kwan                   |image.png |100        |115000     |
    When User ingin menampilkan semua data Buku
    Then User akan menerima semua data buku

  Scenario: Menampilkan Semua data buku dan Tidak ditemukan
    When User ingin mendapatkan semua data buku
    Then User akan menerima pesan Not Found dalam mengambil data buku

  Scenario Outline: Menampilkan Detail Buku dan Ditemukan
    Given Terdapat data Buku
      |isbn           |judulBuku                               |namaPengarang                |coverBuku |sampleBuku |namaPenerbit             |deskripsiBuku            |namaGenre          |jumlahHalaman |tahunTerbit |stockBuku  |hargaBuku  |
      |9781338132311  |Fantastic Beasts and Where to Find Them |J.K. Rowling, Newt Scamander |image.png |image.png  |Arthur A. Levine Books   |Buku Fantasi Beast       |Fantasy            |128           |2017        |200        |110000     |
      |9781974710041  |Jujutsu Kaisen Volume 3                 |Gege Akutami                 |image.png |image.png  |Elex Media Komputindo    |Lanjutan dari volume 2   |Adventure, Fantasy |192           |2020        |50         |100000     |
      |9780804171588  |Crazy Rich Asians                       |Kevin Kwan                   |image.png |image.png  |Sinar Star Book          |                         |Novel              |480           |2014        |100        |115000     |
    When User ingin mendapatkan detail buku berisbn <isbn>
    Then User mendapatkan detail buku berdasarkan <isbn>
    Examples:
      |isbn         |
      |9781338132311|
      |9780804171588|

  Scenario Outline: Menampilkan Detail Buku dan Tidak Ditemukan
    Given Terdapat list data  Buku
      |isbn           |judulBuku                               |namaPengarang                |coverBuku |sampleBuku |namaPenerbit             |deskripsiBuku            |namaGenre          |jumlahHalaman |tahunTerbit |stockBuku  |hargaBuku  |
      |9781338132311  |Fantastic Beasts and Where to Find Them |J.K. Rowling, Newt Scamander |image.png |image.png  |Arthur A. Levine Books   |Buku Fantasi Beast       |Fantasy            |128           |2017        |200        |110000     |
      |9781974710041  |Jujutsu Kaisen Volume 3                 |Gege Akutami                 |image.png |image.png  |Elex Media Komputindo    |Lanjutan dari volume 2   |Adventure, Fantasy |192           |2020        |50         |100000     |
      |9780804171588  |Crazy Rich Asians                       |Kevin Kwan                   |image.png |image.png  |Sinar Star Book          |                         |Novel              |480           |2014        |100        |115000     |
    When User ingin mendapatkan detail buku dengan isbn <isbn>
    Then User mendapatkan pesan Not Found dari pengambilan detail buku <isbn>
    Examples:
      |isbn         |
      |9781338132312|
      |9781338132322|

  Scenario Outline: Menampilkan Data Buku dengan Genre yang sama dan Ditemukan
    Given Terlist Data Detail Buku
      |isbn           |judulBuku                               |namaPengarang                |coverBuku  |sampleBuku |namaPenerbit             |deskripsiBuku            |namaGenre          |jumlahHalaman |tahunTerbit |stockBuku  |hargaBuku  |
      |9781338132311  |Fantastic Beasts and Where to Find Them |J.K. Rowling, Newt Scamander |images.png |images.png |Arthur A. Levine Books   |Buku Fantasi Beast       |Fantasy            |128           |2017        |200        |110000     |
      |9781974710041  |Jujutsu Kaisen Volume 3                 |Gege Akutami                 |images.png |images.png |Elex Media Komputindo    |Lanjutan dari volume 2   |Adventure, Fantasy |192           |2020        |50         |100000     |
      |9780804171588  |Crazy Rich Asians                       |Kevin Kwan                   |images.png |images.png |Sinar Star Book          |                         |Novel              |480           |2014        |100        |115000     |
      |9781338732870  |The Ickabog                             |J.K. Rowling                 |images.png |images.png |Scholastic Inc.          |The Ickbog               |Adventure, Fables  |304           |2020        |20         |200000     |
    When user ingin mendapatkan semua buku dengan genre yang sama dengan isbn <isbn>
    Then User akan menerima data buku <dataisbn> , <judulBuku> , <coverBuku> , <namaPengarang> , <namaPenerbit> , <stockBuku> , <hargaBuku> dengan buku berisbn <isbn>
    Examples:
      |isbn             |dataisbn                      |judulBuku                                              |namaPengarang                                   |coverBuku               |namaPenerbit                              |stockBuku    |hargaBuku          |
      |9781974710041    |9781338732870,  9781338132311 |The Ickabog , Fantastic Beasts and Where to Find Them  |{J.K. Rowling} , {J.K. Rowling, Newt Scamander} |image.png , image.png   |Scholastic Inc , Arthur A. Levine Books   |100 , 200    |200000 , 110000    |
      |9781338732870    |9781974710041                 |Jujutsu Kaisen Volume 3                                |{Gege Akutami}                                  |image.png               |Elex Media Komputindo                     |50           |100000             |

  Scenario Outline: Menampilkan Data Buku dengan Genre yang sama dan Tidak Ditemukan
    Given Terlist Detail Buku
      |isbn           |judulBuku                               |namaPengarang                |coverBuku  |sampleBuku |namaPenerbit             |deskripsiBuku            |namaGenre          |jumlahHalaman     |tahunTerbit |stockBuku  |hargaBuku  |
      |9781338132311  |Fantastic Beasts and Where to Find Them |J.K. Rowling, Newt Scamander |images.png |images.png |Arthur A. Levine Books   |Buku Fantasi Beast       |Fantasy            |128               |2017        |200        |110000     |
      |9781974710041  |Jujutsu Kaisen Volume 3                 |Gege Akutami                 |images.png |images.png |Elex Media Komputindo    |Lanjutan dari volume 2   |Adventure, Fantasy |192               |2020        |50         |100000     |
      |9780804171588  |Crazy Rich Asians                       |Kevin Kwan                   |images.png |images.png |Sinar Star Book          |                         |Novel              |480               |2014        |100        |115000     |
      |9781338732870  |The Ickabog                             |J.K. Rowling                 |images.png |images.png |Scholastic Inc.          |The Ickbog               |Adventure, Fables  |304               |2020        |20         |200000     |
    When user ingin mendapatkan semua buku bergenre yang sama dengan isbn <isbn>
    Then User akan menerima data Not Found dengan request <isbn>
    Examples:
        |isbn            |
        |9780804171588   |

  Scenario Outline: Menampilkan Data Buku berdasarkan Genre tertentu dan Ditemukan
    Given List Data Detail Buku
      |isbn           |judulBuku                                                      |namaPengarang                |coverBuku  |sampleBuku |namaPenerbit             |deskripsiBuku          |namaGenre          |jumlahHalaman |tahunTerbit |stockBuku  |hargaBuku|
      |9781338132311  |Fantastic Beasts and Where to Find Them                        |J.K. Rowling, Newt Scamander |images.png |images.png |Arthur A. Levine Books   |Buku Fantasi Beast     |Fantasy            |128           |2017        |200        |110000   |
      |9781974710041  |Jujutsu Kaisen Volume 3                                        |Gege Akutami                 |images.png |images.png |Elex Media Komputindo    |Lanjutan dari volume 2 |Adventure, Fantasy |192           |2020        |50         |100000   |
      |9780804171588  |Crazy Rich Asians                                              |Kevin Kwan                   |images.png |images.png |Sinar Star Book          |                       |Novel              |480           |2014        |100        |115000   |
      |9781338732870  |The Ickabog                                                    |J.K. Rowling                 |images.png |images.png |Scholastic Inc.          |The Ickbog             |Adventure, Fables  |304           |2020        |20         |200000   |
      |9781423103349  |The Sea of Monsters (Percy Jackson and the Olympians, Book 2)  |Rick Riordan                 |images.png |images.png |Disney-Hyperion          |                       |Fantasy            |320           |2006        |35         |200000   |
    And Terlist Data Genre
      |idGenre     |namaGenre   |
      |G01         |Adventure   |
      |G02         |Fantasy     |
      |G03         |Novel       |
      |G04         |Psikologi   |
    When user ingin mendapatkan semua buku dengan id genre <idGenre>
    Then User akan menerima data buku <isbn> , <judulBuku> , <namaPengarang> , <coverBuku> , <namaPenerbit> , <stockBuku> , <hargaBuku> dari id Genre <idGenre>
    Examples:
      |idGenre |isbn                           |judulBuku                                   |namaPengarang                   |coverBuku                |namaPenerbit                           |stockBuku  |hargaBuku         |
      |G03     |9780804171588                  |Crazy Rich Asians                           |{Kevin Kwan}                    |images.png               |Sinar Star Book                        |100        |115000            |
      |G01     |9781974710041 , 9781338732870  |Jujutsu Kaisen Volume 3 , The Ickabog       |{Gege Akutami} , {J.K. Rowling} |images.png , images.png  |Elex Media Komputindo , Scholastic Inc |50 , 100   |100000 , 200000   |

  Scenario Outline: Menampilkan Data Buku dengan Genre tertentu dan tidak ditemukan
    Given List Detail Buku
      |isbn           |judulBuku                                                      |namaPengarang                |coverBuku  |sampleBuku |namaPenerbit             |deskripsiBuku            |namaGenre          |jumlahHalaman |tahunTerbit |stockBuku  |hargaBuku  |
      |9781338132311  |Fantastic Beasts and Where to Find Them                        |J.K. Rowling, Newt Scamander |images.png |images.png |Arthur A. Levine Books   |Buku Fantasi Beast       |Fantasy            |128           |2017        |200        |110000     |
      |9781974710041  |Jujutsu Kaisen Volume 3                                        |Gege Akutami                 |images.png |images.png |Elex Media Komputindo    |Lanjutan dari volume 2   |Adventure, Fantasy |192           |2020        |50         |100000     |
      |9780804171588  |Crazy Rich Asians                                              |Kevin Kwan                   |images.png |images.png |Sinar Star Book          |                         |Novel              |480           |2014        |100        |115000     |
      |9781338732870  |The Ickabog                                                    |J.K. Rowling                 |images.png |images.png |Scholastic Inc.          |The Ickbog               |Adventure, Fables  |304           |2020        |20         |200000     |
      |9781423103349  |The Sea of Monsters (Percy Jackson and the Olympians, Book 2)  |Rick Riordan                 |images.png |images.png |Disney-Hyperion          |                         |Fantasy            |320           |2006        |35         |200000     |
    And Terlist Genre
      |idGenre     |namaGenre   |
      |G01         |Adventure   |
      |G02         |Fantasy     |
      |G03         |Novel       |
      |G04         |Psikologi   |
    When user ingin mendapatkan semua buku berid genre <idGenre>
    Then User akan menerima Not Found dari buku ber id Genre <idGenre>
    Examples:
      |idGenre|
      |G05    |

  Scenario Outline: Mencari data Buku dan ditemukan
    Given List data detail Buku
      |isbn           |judulBuku                                                      |namaPengarang                |coverBuku  |sampleBuku |namaPenerbit             |deskripsiBuku            |namaGenre          |jumlahHalaman |tahunTerbit |stockBuku  |hargaBuku  |
      |9781338132311  |Fantastic Beasts and Where to Find Them                        |J.K. Rowling, Newt Scamander |images.png |images.png |Arthur A. Levine Books   |Buku Fantasi Beast       |Fantasy            |128           |2017        |200        |110000     |
      |9781974710041  |Jujutsu Kaisen Volume 3                                        |Gege Akutami                 |images.png |images.png |Elex Media Komputindo    |Lanjutan dari volume 2   |Adventure, Fantasy |192           |2020        |50         |100000     |
      |9780804171588  |Crazy Rich Asians                                              |Kevin Kwan                   |images.png |images.png |Sinar Star Book          |                         |Novel              |480           |2014        |100        |115000     |
      |9781338732870  |The Ickabog                                                    |J.K. Rowling                 |images.png |images.png |Scholastic Inc.          |The Ickbog               |Adventure, Fables  |304           |2020        |20         |200000     |
      |9781423103349  |The Sea of Monsters (Percy Jackson and the Olympians, Book 2)  |Rick Riordan                 |images.png |images.png |Disney-Hyperion          |                         |Fantasy            |320           |2006        |35         |200000     |
    When user mencari data buku dengan keyword <keyword>
    Then User akan mendapatkan data buku <isbn> , <judulBuku> , <coverBuku> , <namaPengarang> , <namaPenerbit> , <stockBuku> , <hargaBuku> dengan keyword <keyword>
    Examples:
      |keyword    |isbn                         |judulBuku                                              |namaPengarang                                   |coverBuku            |namaPenerbit                             |stockBuku    |hargaBuku       |
      |beasts     |9781338132311                |Fantastic Beasts and Where to Find Them                |{J.K. Rowling, Newt Scamander}                  |image.png            |Arthur A. Levine Books                   |200          |110000          |
      |rowling    |9781338732870, 9781338132311 |Fantastic Beasts and Where to Find Them, The Ickabog   |{J.K. Rowling, Newt Scamander} , {J.K. Rowling} |image.png, image.png |Arthur A. Levine Books, Scholastic Inc.  |200, 20      |110000, 200000  |

  Scenario Outline: Mencari data buku dan tidak ditemukan
    Given List data-data detail Buku
      |isbn           |judulBuku                                                      |namaPengarang                |coverBuku  |sampleBuku |namaPenerbit             |deskripsiBuku            |namaGenre          |jumlahHalaman |tahunTerbit |stockBuku  |hargaBuku  |
      |9781338132311  |Fantastic Beasts and Where to Find Them                        |J.K. Rowling, Newt Scamander |images.png |images.png |Arthur A. Levine Books   |Buku Fantasi Beast       |Fantasy            |128           |2017        |200        |110000     |
      |9781974710041  |Jujutsu Kaisen Volume 3                                        |Gege Akutami                 |images.png |images.png |Elex Media Komputindo    |Lanjutan dari volume 2   |Adventure, Fantasy |192           |2020        |50         |100000     |
      |9780804171588  |Crazy Rich Asians                                              |Kevin Kwan                   |images.png |images.png |Sinar Star Book          |                         |Novel              |480           |2014        |100        |115000     |
      |9781338732870  |The Ickabog                                                    |J.K. Rowling                 |images.png |images.png |Scholastic Inc.          |The Ickbog               |Adventure, Fables  |304           |2020        |20         |200000     |
      |9781423103349  |The Sea of Monsters (Percy Jackson and the Olympians, Book 2)  |Rick Riordan                 |images.png |images.png |Disney-Hyperion          |                         |Fantasy            |320           |2006        |35         |200000     |
    When user ingin mencari data buku dengan <keyword>
    Then User akan mendapatkan pesan not found dari pencarian buku dengan <keyword>
    Examples:
      |keyword    |
      |Zorro      |