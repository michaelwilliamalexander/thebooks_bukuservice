Feature: Menambahkan Buku

    Scenario Outline: Menambahkan Data Buku dan Berhasil
    Given Terdapat data list Buku
      |isbn           |judulBuku                |namaPengarang  |coverBuku  |sampleBuku  |namaPenerbit             |deskripsiBuku            |namaGenre           |jumlahHalaman|tahunTerbit |stockBuku  |hargaBuku  |
      |9781974710041  |Jujutsu Kaisen Volume 3  |Gege Akutami   |image.png  |image.png   |Elex Media Komputindo    |Lanjutan dari volume 2   |Adventure, Fantasy  |192          |2020        |50         |100000     |
      |9780804171588  |Crazy Rich Asians        |Kevin Kwan     |image.png  |image.png   |Sinar Star Book          |                         |Novel               |480          |2014        |100        |115000     |
    When User menginputkan data buku <isbn> , <judulBuku> , <namaPengarang> , <coverBuku> , <deskripsiBuku> , <sampleBuku> , <namaPenerbit> , <namaGenre> , <tahunTerbit> , <jumlahHalaman> , <hargaBuku> , <stockBuku>
    Then User mendapatkan pesan <pesan> dari data buku <isbn> , <judulBuku> , <namaPengarang> , <coverBuku> , <deskripsiBuku> , <sampleBuku> , <namaPenerbit> , <namaGenre> , <tahunTerbit> , <jumlahHalaman> , <hargaBuku> , <stockBuku>
    Examples:
      |isbn           |judulBuku                                |namaPengarang                |coverBuku |sampleBuku  |namaPenerbit             |deskripsiBuku      |namaGenre         |jumlahHalaman |tahunTerbit |stockBuku  |hargaBuku  |pesan                |
      |9781338132311  |Fantastic Beasts and Where to Find Them  |J.K. Rowling, Newt Scamander |image.png |image.png   |Arthur A. Levine Books   |Buku Fantasi Beast |Fantasy           |128           |2017        |200        |110000     |Penyimpanan Berhasil |
      |9781338132311  |Fantastic Beasts and Where to Find Them  |J.K. Rowling, Newt Scamander |          |image.png   |Arthur A. Levine Books   |Buku Fantasi Beast |Fantasy           |128           |2017        |0          |110000     |Penyimpanan Berhasil |
      |9781338732870  |The Ickabog                              |J.K. Rowling                 |image.png |            |Scholastic Inc           |The Ickbog         |Adventure, Fables |304           |2020        |20         |200000     |Penyimpanan Berhasil |
      |9781338732870  |The Ickabog                              |J.K. Rowling                 |image.png |            |Scholastic Inc           |                   |Adventure, Fables |304           |2020        |20         |200000     |Penyimpanan Berhasil |


  Scenario Outline: Menambahkan Data Buku dan Gagal
    Given Terdapat data list-list Buku
      |isbn           |judulBuku                                 |namaPengarang                |coverBuku |sampleBuku |namaPenerbit             |deskripsiBuku            |namaGenre           |jumlahHalaman     |tahunTerbit |stockBuku  |hargaBuku  |
      |9781338132311  |Fantastic Beasts and Where to Find Them   |J.K. Rowling, Newt Scamander |image.png |image.png  |Arthur A. Levine Books   |Buku Fantasi Beast       |Fantasy             |128               |2017        |200        |110000     |
      |9781974710041  |Jujutsu Kaisen Volume 3                   |Gege Akutami                 |image.png |image.png  |Elex Media Komputindo    |Lanjutan dari volume 2   |Adventure, Fantasy  |192               |2020        |50         |100000     |
      |9780804171588  |Crazy Rich Asians                         |Kevin Kwan                   |image.png |image.png  |Sinar Star Book          |                         |Novel               |480               |2014        |100        |115000     |
    When User memasukkan data buku <isbn> , <judulBuku> , <namaPengarang> , <coverBuku> , <deskripsiBuku> , <sampleBuku> , <namaPenerbit> , <namaGenre> , <tahunTerbit> , <jumlahHalaman> , <hargaBuku> , <stockBuku>
    Then User mendapatkan pesan Bad Request dari data buku <isbn> , <judulBuku> , <namaPengarang> , <coverBuku> , <deskripsiBuku> , <sampleBuku> , <namaPenerbit> , <namaGenre> , <tahunTerbit> , <jumlahHalaman> , <hargaBuku> . <stockBuku> yang diinputkan
    Examples:
      |isbn           |judulBuku                                |namaPengarang                |coverBuku |sampleBuku |namaPenerbit           |deskripsiBuku            |namaGenre          |jumlahHalaman |tahunTerbit |stockBuku  |hargaBuku  |
      |9781338132311  |Fantastic Beasts and Where to Find Them  |J.K. Rowling, Newt Scamander |image.png |           |Arthur A. Levine Books |Buku Fantasi Beast       |Fantasy            |128           |2017        |200        |110000     |
      |               |Fantastic Beasts and Where to Find Them  |J.K. Rowling, Newt Scamander |image.png |image.png  |Arthur A. Levine Books |Buku Fantasi Beast       |Fantasy            |128           |2017        |200        |110000     |
      |9781338732870  |                                         |J.K. Rowling                 |          |           |Scholastic Inc         |The Ickbog               |Adventure, Fables  |304           |2020        |20         |200000     |
      |               |                                         |J.K. Rowling                 |          |           |Scholastic Inc         |The Ickbog               |Adventure, Fables  |304           |2020        |20         |200000     |
