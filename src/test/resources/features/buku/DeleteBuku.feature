Feature: Delete Buku

  Scenario Outline:Menghapus Data Buku dan Berhasil
    Given Terdapat list data buku
      |isbn           |judulBuku                               |namaPengarang                |coverBuku  |sampleBuku  |namaPenerbit             |deskripsiBuku            |namaGenre          |jumlahHalaman |tahunTerbit |stockBuku  |hargaBuku  |
      |9781338132311  |Fantastic Beasts and Where to Find Them |J.K. Rowling, Newt Scamander |image.png  |image.png   |Arthur A. Levine Books   |Buku Fantasi Beast       |Fantasy            |128           |2017        |200        |110000     |
      |9781974710041  |Jujutsu Kaisen Volume 3                 |Gege Akutami                 |image.png  |image.png   |Elex Media Komputindo    |Lanjutan dari volume 2   |Adventure, Fantasy |192           |2020        |50         |100000     |
      |9780804171588  |Crazy Rich Asians                       |Kevin Kwan                   |image.png  |image.png   |Sinar Star Book          |                         |Novel              |480           |2014        |100        |115000     |
    When User menghapus data buku berdasarkan <isbn>
    Then User akan mendapatkan <pesan> dari isbn <isbn> yang dihapus
    Examples:
      |isbn           |pesan                      |
      |9781338132311  |Delete Berhasil            |
      |9781974710041  |Delete Berhasil            |

  Scenario Outline:Menghapus Data Buku dan Gagal
    Given list data buku
      |isbn           |judulBuku                               |namaPengarang                |coverBuku  |sampleBuku  |namaPenerbit             |deskripsiBuku            |namaGenre          |jumlahHalaman |tahunTerbit |stockBuku  |hargaBuku  |
      |9781338132311  |Fantastic Beasts and Where to Find Them |J.K. Rowling, Newt Scamander |image.png  |image.png   |Arthur A. Levine Books   |Buku Fantasi Beast       |Fantasy            |128           |2017        |200        |110000     |
      |9781974710041  |Jujutsu Kaisen Volume 3                 |Gege Akutami                 |image.png  |image.png   |Elex Media Komputindo    |Lanjutan dari volume 2   |Adventure, Fantasy |192           |2020        |50         |100000     |
      |9780804171588  |Crazy Rich Asians                       |Kevin Kwan                   |image.png  |image.png   |Sinar Star Book          |                         |Novel              |480           |2014        |100        |115000     |
    When User menghapus data buku menggunakan id <isbn>
    Then User akan menerima pesan Bad Request dari penghapusan data <isbn>
    Examples:
      |isbn           |
      |9781974710042  |
      |B03            |