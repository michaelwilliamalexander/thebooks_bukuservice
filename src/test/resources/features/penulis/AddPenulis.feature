Feature:Menambahkan Penulis

  Scenario Outline: Menambahkan data Penulis dan Berhasil
    Given Terdapat data penulis
      |idPenulis      |namaPenulis          |
      |PE1            |Kristin Hannah       |
      |PE2            |Matt Haig            |
      |PE3            |Kazuo Ishiguro       |
    When User memasukkan data penulis <idPenulis> dan <namaPenulis>
    Then User akan memperoleh pesan <pesan> dari data penulis <idPenulis> dan <namaPenulis>
    Examples:
      |idPenulis  |namaPenulis                |pesan                |
      |PE4        |Joanne Kathleen Rowling    |Penyimpanan Berhasil |
      |PE12       |J.K.Rowling                |Penyimpanan Berhasil |

    Scenario Outline: Menambahkan data Penulis dan gagal
      Given Terdapat data list penulis
        |idPenulis      |namaPenulis          |
        |PE1            |Kristin Hannah       |
        |PE2            |Matt Haig            |
        |PE3            |Kazuo Ishiguro       |
        |PE4            |J.K.Rowling          |
      When User menginputkan data penulis <idPenulis> dan <namaPenulis>
      Then User akan mendapatkan pesan Bad Request dari data penulis <idPenulis> dan <namaPenulis>
      Examples:
      |idPenulis  |namaPenulis          |
      |PE5        |Kazuo Ishiguro       |
      |PE1        |Matt Haig            |
      |           |J.K.Rowling          |
      |PE0001     |                     |
      |           |                     |





