Feature: Mendapatkan Data Penulis

  Scenario:Menampilkan Semua data penulis
    Given Terdapat data-data penulis
      |idPenulis      |namaPenulis          |
      |PE1            |Kristin Hannah       |
      |PE2            |Matt Haig            |
      |PE3            |Kazuo Ishiguro       |
      |PE4            |J.K.Rowling          |
    When User ingin mendapatkan semua data penulis
    Then User akan mendapatkan data penulis

  Scenario: User mendapatkan semua data penulis dan Tidak Ditemukan
    When User ingin mendapatkan data penulis
    Then User akan menerima pesan Not Found dalam mengambil data penulis