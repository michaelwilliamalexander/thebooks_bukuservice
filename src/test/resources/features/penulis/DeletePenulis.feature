Feature: Delete Penulis

  Scenario Outline:Menghapus data Penulis dan Berhasil
    Given Terdapat list data penulis
      |idPenulis      |namaPenulis          |
      |PE1            |Kristin Hannah       |
      |PE2            |Matt Haig            |
      |PE3            |Kazuo Ishiguro       |
      |PE4            |J.K.Rowling          |
    When User ingin menghapus data penulis dengan id <idPenulis>
    Then User mendapatkan pesan <pesan> dari id Penulis <idPenulis>
    Examples:
      |idPenulis  |pesan                  |
      |PE1        |Delete Berhasil        |
      |PE2        |Delete Berhasil        |

  Scenario Outline:Menghapus data Penulis dan Gagal

    Given Terdapat list data-data penulis
      |idPenulis      |namaPenulis          |
      |PE1            |Kristin Hannah       |
      |PE2            |Matt Haig            |
      |PE3            |Kazuo Ishiguro       |
      |PE4            |J.K.Rowling          |
    When User ingin menghapus data penulis dengan menggunakan <idPenulis>
    Then User menerima pesan Bad Request dari id Penulis <idPenulis>
    Examples:
      |idPenulis  |
      |PE5        |
      |ABC        |