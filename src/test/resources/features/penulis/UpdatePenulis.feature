Feature: Update Penulis

  Scenario Outline: Memperbarui data Penulis dan Berhasil
    Given Data list penulis
      |idPenulis      |namaPenulis          |
      |PE1            |Kristin Hannah       |
      |PE2            |Matt Haig            |
      |PE3            |Kazuo Ishiguro       |
      |PE4            |J.K.Rowling          |
    When User mencari data penulis dengan menggunakan <idPenulis> dan menginputkan data penulis <namaPenulis>
    Then User akan mendapatkan pesan <pesan> dari data update <idPenulis> dan <namaPenulis>
    Examples:
      |idPenulis  |namaPenulis                |pesan                           |
      |PE4        |Joanne Kathleen Rowling    |Update Berhasil                 |


  Scenario Outline: Memperbarui data Penulis dan Gagal
    Given Terdapat list penulis
      |idPenulis      |namaPenulis          |
      |PE1            |Kristin Hannah       |
      |PE2            |Matt Haig            |
      |PE3            |Kazuo Ishiguro       |
      |PE4            |J.K.Rowling          |
    When User mencari data penulis dengan menggunakan <idPenulis> dan memasukkan data penulis <namaPenulis>
    Then User akan menerima pesan Bad Request dari data update <idPenulis> dan <namaPenulis>
    Examples:
      |idPenulis  |namaPenulis            |
      |PE02       |Kazuo Ishiguro         |
      |PE01       |                       |