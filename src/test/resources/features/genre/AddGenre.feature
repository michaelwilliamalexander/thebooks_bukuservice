Feature: Add Genre

  Scenario Outline: Menambahkan data Genre baru dan Berhasil
    Given Terdapat data genre di dalam sistem
      |idGenre     |namaGenre   |
      |G01         |Dongeng     |
      |G02         |Filsafat    |
      |G03         |Esai        |
      |G04         |Psikologi   |
    When User menginputkan data genre <idGenre> dan <namaGenre>
    Then User akan menerima <pesan> dari data genre <idGenre> dan <namaGenre>
    Examples:
      |idGenre        |namaGenre      |pesan                |
      |G05            |Autobiografi   |Penyimpanan Berhasil |
      |G06            |Novel          |Penyimpanan Berhasil |


  Scenario Outline: Menambahkan data Genre baru dan Gagal
    Given Terdapat data-data list genre di dalam sistem
      |idGenre     |namaGenre   |
      |G01         |Dongeng     |
      |G02         |Filsafat    |
      |G03         |Esai        |
      |G04         |Psikologi   |
    When User memasukkan data genre <idGenre> dan <namaGenre>
    Then Sistem akan mengembalikan Bad Request dari data genre <idGenre> dan <namaGenre> yang dimasukkan
    Examples:
      |idGenre        |namaGenre      |
      |G03            |Autobiografi   |
      |G05            |Psikologi      |
      |G05            |               |
      |               |Novel          |
      |               |               |