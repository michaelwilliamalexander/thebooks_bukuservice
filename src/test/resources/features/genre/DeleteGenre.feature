Feature: Delete Genre

  Scenario Outline: Menghapus data Genre dan Berhasil
    Given Terdapat data Genre
      |idGenre     |namaGenre   |
      |G01         |Dongeng     |
      |G02         |Filsafat    |
      |G03         |Esai        |
      |G04         |Psikologi   |
    When User menghapus data genre <idGenre>
    Then User menerima pesan <pesan> dari genre berId <idGenre>
    Examples:
      |idGenre        |pesan                  |
      |G01            |Delete Berhasil        |
      |G03            |Delete Berhasil        |


  Scenario Outline: Menghapus data Genre dan Gagal
    Given Terdapat data list-list Genre
      |idGenre     |namaGenre   |
      |G01         |Dongeng     |
      |G02         |Filsafat    |
      |G03         |Esai        |
      |G04         |Psikologi   |
    When User menghapus genre <idGenre>
    Then User menerima pesan Bad Request dengan id genre <idGenre>
    Examples:
      |idGenre        |
      |G05            |
      |ABC            |