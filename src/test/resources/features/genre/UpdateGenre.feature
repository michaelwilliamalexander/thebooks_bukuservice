Feature: Update Genre

  Scenario Outline: Mengupdate data Genre dan Berhasil
    Given Terdapat data genre
      |idGenre     |namaGenre   |
      |G01         |Dongeng     |
      |G02         |Filsafat    |
      |G03         |Esai        |
      |G04         |Psikologi   |
    When User mempebarui data genre dengan id <idGenre> dengan data <genreBaru>
    Then User mendapatkan pesan <pesan> dari data update <idGenre> dan <genreBaru>
    Examples:
      |idGenre      |genreBaru        |pesan               |
      |G01          |Novel            |Update Berhasil     |

  Scenario Outline: Mengupdate data Genre dan Gagal
    Given Terdapat data list genre
      |idGenre     |namaGenre   |
      |G01         |Dongeng     |
      |G02         |Filsafat    |
      |G03         |Esai        |
      |G04         |Psikologi   |
    When User mempebarui genre ber id <idGenre> dengan data <genreBaru>
    Then User mendapatkan pesan Bad Request dari data <idGenre> dan <genreBaru>
    Examples:
      |idGenre      |genreBaru        |
      |G04          |                 |
      |G04          |Esai             |
      |G05          |Novel            |