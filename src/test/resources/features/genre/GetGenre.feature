Feature: Mendapatkan data Genre

  Scenario: Mendapatkan Semua data Genre dan Ditemukan
    Given Tedapat data-data genre
      |idGenre     |namaGenre   |
      |G01         |Dongeng     |
      |G02         |Filsafat    |
      |G03         |Esai        |
      |G04         |Psikologi   |
    When User ingin menampilkan semua data genre
    Then User mendapatkan semua data genre

  Scenario: Mendapatkan Semua data Genre dan Tidak Ditemukan
    When User menampilkan semua data genre
    Then User mendapatkan pesan Not Found terhadap data genre



