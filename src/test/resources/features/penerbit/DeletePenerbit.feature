Feature: Fitur Menghapus Data Penerbit

  Scenario Outline: User Ingin Menghapus data penerbit dan Berhasil
    Given Terdapat data list penerbit
      |idPenerbit     |namaPenerbit       |
      |P01            |Erlangga           |
      |P02            |Kompas             |
      |P03            |Gramedia           |
    When User memasukkan id Penerbit <idPenerbit>
    Then User mendapatkan pesan <pesan> dari id Penerbit <idPenerbit>
    Examples:
      |idPenerbit  |pesan            |
      |P01         |Delete Berhasil  |
      |P02         |Delete Berhasil  |

  Scenario Outline: User Ingin Menghapus data penerbit dan data tidak ditemukan
    Given Terdapat data penerbit
      |idPenerbit     |namaPenerbit       |
      |P01            |Erlangga           |
      |P02            |Kompas             |
      |P03            |Gramedia           |
    When User menginputkan id Penerbit <idPenerbit>
    Then User mendapatkan data not found dari data Penerbit <idPenerbit>
    Examples:
      |idPenerbit  |
      |P12         |
