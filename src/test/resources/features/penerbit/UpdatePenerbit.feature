Feature: Fitur Update Penerbit

  Scenario Outline: User memperbarui data penerbit dan Berhasil
    Given Terdapat data-data penerbit
      |idPenerbit     |namaPenerbit       |
      |P01            |Erlangga           |
      |P02            |Kompas             |
      |P03            |Gramedia           |
    When User memperbarui data <idPenerbit> dan menginputkan data penerbit <updateNamaPenerbit>
    Then User akan menerima pesan <pesan> dari data penerbit <idPenerbit> , <updateNamaPenerbit> yang diupdate
    Examples:
      |idPenerbit   |updateNamaPenerbit   |pesan             |
      |P01          |TransMedia           |Update Berhasil   |
      |P02          |Kompas               |Update Berhasil   |

  Scenario Outline: User memperbarui data penerbit dan Gagal
    Given Terdapat data-data list penerbit
      |idPenerbit     |namaPenerbit       |
      |P01            |Erlangga           |
      |P02            |Kompas             |
      |P03            |Gramedia           |
    When User memngupdate id penerbit <idPenerbit> dan menginputkan data <updateNamaPenerbit>
    Then User akan menerima Bad request dari data update penerbit <idPenerbit> , <updateNamaPenerbit>
    Examples:
      |idPenerbit   |updateNamaPenerbit   |
      |P03          |                     |
      |P04          |                     |
      |P02          |Gramedia             |