Feature: Fitur Menambah Penerbit

  Scenario Outline: User menambahkan data penerbit baru dan Berhasil
    Given Terdapat beberapa data penerbit
      |idPenerbit     |namaPenerbit       |
      |P01            |Erlangga           |
      |P02            |Kompas             |
      |P03            |Gramedia           |
    When User akan menginputkan data penerbit <idPenerbit> , <namaPenerbit>
    Then User akan mendapatkan pesan <pesan> dari data penerbit <idPenerbit> , <namaPenerbit>
    Examples:
      |idPenerbit   |namaPenerbit   |pesan                   |
      |P04          |TransMedia     |Penyimpanan Berhasil    |
      |P12          |TransMedia     |Penyimpanan Berhasil    |

  Scenario Outline: User menambahkan data penerbit baru dan Gagal
    Given Terdapat beberapa data-data penerbit
      |idPenerbit     |namaPenerbit       |
      |P01            |Erlangga           |
      |P02            |Kompas             |
      |P03            |Gramedia           |
    When User akan memasukkan data penerbit <idPenerbit> , <namaPenerbit>
    Then User akan mendapatkan pesan Bad Request dari inputan data penerbit <idPenerbit> , <namaPenerbit>
    Examples:
      |idPenerbit   |namaPenerbit   |
      |P04          |Erlangga       |
      |P01          |Transmedia     |
      |P04          |               |
      |             |Kompas         |
      |             |               |