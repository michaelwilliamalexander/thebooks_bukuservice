Feature: Fitur mendapatkan data Penerbit

  Scenario: User mendapatkan semua data penerbit dan Ditemukan
    Given Terdapat data-data penerbit di dalam sistem
      |idPenerbit     |namaPenerbit       |
      |P01            |Erlangga           |
      |P02            |Kompas             |
      |P03            |Gramedia           |
    When User ingin menampilkan data penerbit
    Then User akan mendapatkan semua data penerbit

  Scenario: User mendapatkan semua data penerbit dan Tidak Ditemukan
    When User ingin mendapatkan data penerbit
    Then User akan menerima pesan Not Found dalam mengambil data penerbit






