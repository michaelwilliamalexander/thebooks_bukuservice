package id.ac.ukdw.thebooksbookservice.features.penerbit.add;

import id.ac.ukdw.thebooksbookservice.config.ObjectMapping;
import id.ac.ukdw.thebooksbookservice.controller.PenerbitController;
import id.ac.ukdw.thebooksbookservice.dto.request.PenerbitRequest;
import id.ac.ukdw.thebooksbookservice.model.Penerbit;
import id.ac.ukdw.thebooksbookservice.service.PenerbitService;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class AddPenerbitTest {

    @InjectMocks
    private PenerbitController controller;

    @Mock
    private PenerbitService service;

    private MockMvc mockMvc;

    private List<Penerbit> penerbits;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        penerbits = new ArrayList<>();
    }
    
    @Given("^Terdapat beberapa data penerbit$")
    public void terdapat_beberapa_data_penerbit(List<Penerbit> penerbitList) {
        penerbits = penerbitList;
    }

    @When("^User akan menginputkan data penerbit (.*) , (.*)$")
    public void input(final String idPenerbit, final String namaPenerbit) {
        when(service.addPenerbit(idPenerbit,namaPenerbit))
                .thenReturn("Penyimpanan Berhasil");

    }
    @Then("^User akan mendapatkan pesan (.*) dari data penerbit (.*) , (.*)$")
    public void output(final String pesan, final String idPenerbit,
                       final String namaPenerbit) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/buku/penerbit/")
                .content(ObjectMapping.asJsonString(
                        new PenerbitRequest(idPenerbit,namaPenerbit)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data",is(pesan))).andDo(print());

        verify(service).addPenerbit(idPenerbit,namaPenerbit);
    }
}
