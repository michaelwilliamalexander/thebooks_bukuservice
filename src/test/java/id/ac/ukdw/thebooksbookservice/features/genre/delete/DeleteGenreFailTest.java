package id.ac.ukdw.thebooksbookservice.features.genre.delete;

import id.ac.ukdw.thebooksbookservice.controller.GenreController;
import id.ac.ukdw.thebooksbookservice.exception.BadRequestException;
import id.ac.ukdw.thebooksbookservice.model.Genre;
import id.ac.ukdw.thebooksbookservice.service.GenreService;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class DeleteGenreFailTest {

    @InjectMocks
    private GenreController controller;

    @Mock
    private GenreService services;

    private MockMvc mockMvc;

    private List<Genre> genres;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        genres = new ArrayList<>();
    }

    @Given("^Terdapat data list-list Genre$")
    public void data(List<Genre> genreList) {
        genres = genreList;
    }

    @When("^User menghapus genre (.*)$")
    public void input(final String idGenre) {
        when(services.deleteGenre(idGenre)).thenThrow(new BadRequestException());
    }
    @Then("^User menerima pesan Bad Request dengan id genre (.*)$")
    public void output(final String idGenre) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/buku/genre/{id}",idGenre))
                .andExpect(status().isBadRequest())
                .andDo(print());

        verify(services).deleteGenre(idGenre);
    }
}
