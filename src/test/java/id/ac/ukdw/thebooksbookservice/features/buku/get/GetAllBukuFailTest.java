package id.ac.ukdw.thebooksbookservice.features.buku.get;

import id.ac.ukdw.thebooksbookservice.controller.BukuController;
import id.ac.ukdw.thebooksbookservice.exception.NotFoundException;
import id.ac.ukdw.thebooksbookservice.service.BukuService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GetAllBukuFailTest {

    @InjectMocks
    private BukuController controller;

    @Mock
    private BukuService service;

    private MockMvc mockMvc;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @When("User ingin mendapatkan semua data buku")
    public void input() {
        when(service.getAllBuku()).thenThrow(new NotFoundException());
    }

    @Then("User akan menerima pesan Not Found dalam mengambil data buku")
    public void output() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/buku/"))
                .andExpect(status().isNotFound());

        verify(service).getAllBuku();
    }
}
