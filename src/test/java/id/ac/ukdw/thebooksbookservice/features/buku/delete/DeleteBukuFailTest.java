package id.ac.ukdw.thebooksbookservice.features.buku.delete;

import id.ac.ukdw.thebooksbookservice.controller.BukuController;
import id.ac.ukdw.thebooksbookservice.dto.response.DetailBukuResponse;
import id.ac.ukdw.thebooksbookservice.exception.BadRequestException;
import id.ac.ukdw.thebooksbookservice.model.TableDetailBuku;
import id.ac.ukdw.thebooksbookservice.service.BukuService;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.SneakyThrows;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class DeleteBukuFailTest {
    @InjectMocks
    private BukuController controller;

    @Mock
    private BukuService service;

    private MockMvc mockMvc;

    private List<DetailBukuResponse> dataBuku;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataBuku = new ArrayList<>();
    }

    @Given("^list data buku$")
    public void data(List<TableDetailBuku> data) {
        for (TableDetailBuku item: data){
            List<String>pengarang = Arrays.asList(item.getNamaPengarang().split("\\s*,\\s*"));
            List<String>genre = Arrays.asList(item.getNamaGenre().split("\\s*,\\s*"));
            dataBuku.add(new DetailBukuResponse(
                    item.getIsbn(),
                    item.getJudulBuku(),
                    pengarang,
                    genre,
                    item.getJumlahHalaman(),
                    item.getCoverBuku().getBytes(),
                    item.getSampleBuku().getBytes(),
                    item.getDeskripsiBuku(),
                    item.getTahunTerbit(),
                    Integer.valueOf(item.getJumlahHalaman()),
                    Double.valueOf(item.getHargaBuku()),
                    item.getNamaPenerbit()
            ));
        }
    }

    @When("^User menghapus data buku menggunakan id (.*)$")
    public void input(final String isbn) {
        when(service.deleteBuku(isbn)).thenThrow(new BadRequestException());
    }

    @SneakyThrows
    @Then("^User akan menerima pesan Bad Request dari penghapusan data (.*)$")
    public void output(final String isbn) {

        mockMvc.perform(MockMvcRequestBuilders.delete("/buku/{id}",isbn))
                .andDo(print())
                .andExpect(status().isBadRequest());

        verify(service).deleteBuku(isbn);
    }
}
