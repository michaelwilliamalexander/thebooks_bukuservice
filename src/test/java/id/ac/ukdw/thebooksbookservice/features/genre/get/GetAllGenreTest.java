package id.ac.ukdw.thebooksbookservice.features.genre.get;

import id.ac.ukdw.thebooksbookservice.controller.GenreController;
import id.ac.ukdw.thebooksbookservice.dto.GenreDTO;
import id.ac.ukdw.thebooksbookservice.model.Genre;
import id.ac.ukdw.thebooksbookservice.service.GenreService;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetAllGenreTest {

    @InjectMocks
    private GenreController controller;

    @Mock
    private GenreService services;

    private MockMvc mockMvc;

    private List<GenreDTO> genres;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        genres = new ArrayList<>();
    }

    @Given("^Tedapat data-data genre$")
    public void data(List<GenreDTO> genreList) {
        genres = genreList;
    }

    @When("^User ingin menampilkan semua data genre$")
    public void input() {
        Mockito.when(services.getAllGenre()).thenReturn(genres);
    }

    @Then("^User mendapatkan semua data genre$")
    public void output() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/buku/genre/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data",hasSize(genres.size())))
                .andDo(print());

        verify(services).getAllGenre();
    }



}
