package id.ac.ukdw.thebooksbookservice.features.buku.get;

import id.ac.ukdw.thebooksbookservice.config.ObjectMapping;
import id.ac.ukdw.thebooksbookservice.controller.BukuController;
import id.ac.ukdw.thebooksbookservice.dto.BukuDTO;
import id.ac.ukdw.thebooksbookservice.dto.response.BukuResponse;
import id.ac.ukdw.thebooksbookservice.dto.response.ResponseWrapper;
import id.ac.ukdw.thebooksbookservice.model.Buku;
import id.ac.ukdw.thebooksbookservice.model.TableBuku;
import id.ac.ukdw.thebooksbookservice.service.BukuService;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetAllBukuTest {

    @InjectMocks
    private BukuController controller;

    @Mock
    private BukuService service;

    private MockMvc mockMvc;

    private List<BukuResponse> dataBuku;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataBuku = new ArrayList<>();
    }

    @Given("Terdapat data-data buku")
    public void data(List<TableBuku> data) {
        for (TableBuku item:data){
            List<String> pengarang = Arrays.asList(item.getNamaPengarang().split("\\s*,\\s*"));
            dataBuku.add(new BukuResponse(
                    item.getIsbn(),
                    item.getJudulBuku(),
                    pengarang,
                    item.getCoverBuku().getBytes(),
                    item.getStockBuku(),
                    item.getHargaBuku()
            ));
        }
    }

    @When("User ingin menampilkan semua data Buku")
    public void input() {
        when(service.getAllBuku()).thenReturn(dataBuku);
    }

    @Then("User akan menerima semua data buku")
    public void output() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/buku/"))
                .andExpect(jsonPath("$.data",hasSize(dataBuku.size())))
                .andExpect(status().isOk())
                .andExpect(content().bytes(
                        ObjectMapping.asJsonString(new ResponseWrapper(dataBuku)).getBytes()))
                .andDo(print());

        verify(service).getAllBuku();
    }
}
