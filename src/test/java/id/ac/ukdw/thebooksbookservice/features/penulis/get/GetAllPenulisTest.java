package id.ac.ukdw.thebooksbookservice.features.penulis.get;

import id.ac.ukdw.thebooksbookservice.controller.PenulisController;
import id.ac.ukdw.thebooksbookservice.dto.PenulisDTO;
import id.ac.ukdw.thebooksbookservice.model.Penulis;
import id.ac.ukdw.thebooksbookservice.service.PenulisService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetAllPenulisTest {

    @InjectMocks
    private PenulisController controller;

    @Mock
    private PenulisService service;

    private MockMvc mockMvc;

    private List<PenulisDTO> penulis;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        penulis = new ArrayList<>();
    }

    @Given("Terdapat data-data penulis")
    public void data(List<PenulisDTO> data) {
        penulis = data;
    }

    @When("User ingin mendapatkan semua data penulis")
    public void input() {
        when(service.getAllPenulis()).thenReturn(penulis);
    }
    @Then("User akan mendapatkan data penulis")
    public void output() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/buku/penulis/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data",hasSize(penulis.size())))
                .andExpect(jsonPath("$.data[0].idPenulis",
                        is(penulis.get(0).getIdPenulis())))
                .andExpect(jsonPath("$.data[0].namaPenulis",
                        is(penulis.get(0).getNamaPenulis())))
                .andDo(print());

        verify(service).getAllPenulis();
    }
}
