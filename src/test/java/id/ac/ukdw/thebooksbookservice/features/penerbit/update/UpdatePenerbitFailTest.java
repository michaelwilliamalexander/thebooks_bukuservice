package id.ac.ukdw.thebooksbookservice.features.penerbit.update;

import id.ac.ukdw.thebooksbookservice.config.ObjectMapping;
import id.ac.ukdw.thebooksbookservice.controller.PenerbitController;
import id.ac.ukdw.thebooksbookservice.dto.request.PenerbitRequest;
import id.ac.ukdw.thebooksbookservice.exception.BadRequestException;
import id.ac.ukdw.thebooksbookservice.model.Penerbit;
import id.ac.ukdw.thebooksbookservice.service.PenerbitService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UpdatePenerbitFailTest {

    @InjectMocks
    private PenerbitController controller;

    @Mock
    private PenerbitService service;

    private MockMvc mockMvc;

    private List<Penerbit> penerbits;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        penerbits = new ArrayList<>();
    }

    @Given("^Terdapat data-data list penerbit$")
    public void data(List<Penerbit> data) {
        penerbits = data;
    }

    @When("^User memngupdate id penerbit (.*) dan menginputkan data (.*)$")
    public void input(final String idPenerbit, final String namaPenerbit) {
        when(service.updatePenerbit(idPenerbit,namaPenerbit))
                .thenThrow(new BadRequestException());
    }
    @Then("^User akan menerima Bad request dari data update penerbit (.*) , (.*)$")
    public void output(final String idPenerbit, final String namaPenerbit) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.put("/buku/penerbit/{id}",idPenerbit)
                .content(ObjectMapping.asJsonString(
                        new PenerbitRequest(idPenerbit,namaPenerbit)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verify(service).updatePenerbit(idPenerbit,namaPenerbit);
    }
}
