package id.ac.ukdw.thebooksbookservice.features.penerbit.delete;

import id.ac.ukdw.thebooksbookservice.controller.PenerbitController;
import id.ac.ukdw.thebooksbookservice.model.Penerbit;
import id.ac.ukdw.thebooksbookservice.service.PenerbitService;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DeletePenerbitTest {

    @InjectMocks
    private PenerbitController controller;

    @Mock
    private PenerbitService service;

    private MockMvc mockMvc;

    private List<Penerbit> penerbits;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        penerbits = new ArrayList<>();
    }

    @Given("^Terdapat data list penerbit$")
    public void data(List<Penerbit> penerbitList) {
        penerbits = penerbitList;
    }

    @When("^User memasukkan id Penerbit (.*)$")
    public void input(final String idPenerbit) {
        when(service.deletePenerbit(idPenerbit)).thenReturn("Delete Berhasil");
    }
    @Then("^User mendapatkan pesan (.*) dari id Penerbit (.*)$")
    public void output(final String pesan, final String idPenerbit) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/buku/penerbit/{id}",idPenerbit))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data",is(pesan)))
                .andDo(print());

        verify(service).deletePenerbit(idPenerbit);
    }

}
