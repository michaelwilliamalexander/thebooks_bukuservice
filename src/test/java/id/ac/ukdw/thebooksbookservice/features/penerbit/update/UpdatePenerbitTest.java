package id.ac.ukdw.thebooksbookservice.features.penerbit.update;

import id.ac.ukdw.thebooksbookservice.config.ObjectMapping;
import id.ac.ukdw.thebooksbookservice.controller.PenerbitController;
import id.ac.ukdw.thebooksbookservice.dto.request.PenerbitRequest;
import id.ac.ukdw.thebooksbookservice.dto.request.UpdatePenerbitRequest;
import id.ac.ukdw.thebooksbookservice.model.Penerbit;
import id.ac.ukdw.thebooksbookservice.service.PenerbitService;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UpdatePenerbitTest {

    @InjectMocks
    private PenerbitController controller;

    @Mock
    private PenerbitService service;

    private MockMvc mockMvc;

    private List<Penerbit> penerbits;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        penerbits = new ArrayList<>();
    }

    @Given("^Terdapat data-data penerbit$")
    public void data(List<Penerbit> penerbitList) {
        penerbits = penerbitList;
    }

    @When("^User memperbarui data (.*) dan menginputkan data penerbit (.*)$")
    public void input(final String idPenerbit, final String namaPenerbit) {
        when(service.updatePenerbit(idPenerbit,namaPenerbit)).thenReturn("Update Berhasil");
    }
    @Then("^User akan menerima pesan (.*) dari data penerbit (.*) , (.*) yang diupdate$")
    public void output(final String pesan,
                       final String idPenerbit,
                       final String namaPenerbit) throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.put("/buku/penerbit/{id}",idPenerbit)
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapping.asJsonString(new UpdatePenerbitRequest(namaPenerbit))))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.data",is(pesan)));
        
        verify(service).updatePenerbit(idPenerbit,namaPenerbit);
    }
}
