package id.ac.ukdw.thebooksbookservice.features.buku.get;

import id.ac.ukdw.thebooksbookservice.controller.BukuController;
import id.ac.ukdw.thebooksbookservice.dto.response.BukuResponse;
import id.ac.ukdw.thebooksbookservice.dto.response.DetailBukuResponse;
import id.ac.ukdw.thebooksbookservice.model.TableDetailBuku;
import id.ac.ukdw.thebooksbookservice.service.BukuService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetBukuBySimilarGenre {

    @InjectMocks
    private BukuController controller;

    @Mock
    private BukuService service;

    private MockMvc mockMvc;

    private List<DetailBukuResponse> dataBuku;

    private List<BukuResponse> response;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataBuku = new ArrayList<>();
        response = new ArrayList<>();
    }

    @Given("^Terlist Data Detail Buku$")
    public void data(List<TableDetailBuku> data) {
        for (TableDetailBuku item: data){
            List<String>pengarang = Arrays.asList(item.getNamaPengarang().split("\\s*,\\s*"));
            List<String>genre = Arrays.asList(item.getNamaGenre().split("\\s*,\\s*"));
            dataBuku.add(new DetailBukuResponse(
                    item.getIsbn(),
                    item.getJudulBuku(),
                    pengarang,
                    genre,
                    item.getJumlahHalaman(),
                    item.getCoverBuku().getBytes(),
                    item.getSampleBuku().getBytes(),
                    item.getDeskripsiBuku(),
                    item.getTahunTerbit(),
                    Integer.valueOf(item.getJumlahHalaman()),
                    Double.valueOf(item.getHargaBuku()),
                    item.getNamaPenerbit()
            ));
        }
    }

    @When("^user ingin mendapatkan semua buku dengan genre yang sama dengan isbn (.*)$")
    public void input(final String isbn) {
        for (DetailBukuResponse item:dataBuku){
            if (item.getIsbn().equals(isbn)){
                List<String> genres = item.getGenreBuku();
                for (String genre:genres){
                    for (DetailBukuResponse dataResponse: dataBuku){
                        if (!dataResponse.getIsbn().equals(isbn)){
                            for (String items: dataResponse.getGenreBuku()){
                                if (items.equals(genre)){
                                    response.add(new BukuResponse(
                                            dataResponse.getIsbn(),
                                            dataResponse.getJudulBuku(),
                                            dataResponse.getNamaPengarang(),
                                            dataResponse.getCoverBuku(),
                                            dataResponse.getStockBuku(),
                                            dataResponse.getHargaBuku()
                                    ));
                                    break;
                                }
                            }
                        }
                    }
                }
                when(service.getSameGenreByIsbn(isbn)).thenReturn(response);
            }
        }
    }

    @Then("^User akan menerima data buku (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) dengan buku berisbn (.*)$")
    public void output(final String listIsbn,
                       final String judulBuku,
                       final String coverBuku,
                       final String namaPengarang,
                       final String namaPenerbit,
                       final String stockBuku,
                       final String hargaBuku,
                       final String isbn) throws Exception{

        mockMvc.perform(MockMvcRequestBuilders.get("/buku/{isbn}",isbn)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data",hasSize(response.size())))
                .andDo(print())
                .andExpect(status().isOk());

        verify(service).getSameGenreByIsbn(isbn);
    }

}
