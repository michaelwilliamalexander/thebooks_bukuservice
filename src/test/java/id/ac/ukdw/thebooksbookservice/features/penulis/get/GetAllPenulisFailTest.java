package id.ac.ukdw.thebooksbookservice.features.penulis.get;

import id.ac.ukdw.thebooksbookservice.controller.PenulisController;
import id.ac.ukdw.thebooksbookservice.exception.NotFoundException;
import id.ac.ukdw.thebooksbookservice.model.Penulis;
import id.ac.ukdw.thebooksbookservice.service.PenulisService;
import io.cucumber.java.Before;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetAllPenulisFailTest {

    @InjectMocks
    private PenulisController controller;

    @Mock
    private PenulisService service;

    private MockMvc mockMvc;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @When("User ingin mendapatkan data penulis")
    public void input(){
        when(service.getAllPenulis()).thenThrow(new NotFoundException());
    }

    @Then("User akan menerima pesan Not Found dalam mengambil data penulis")
    public void output() throws Exception {
        
        mockMvc.perform(MockMvcRequestBuilders.get("/buku/penulis/"))
                .andExpect(status().isNotFound())
                .andDo(print());
        
        verify(service).getAllPenulis();
    }
}
