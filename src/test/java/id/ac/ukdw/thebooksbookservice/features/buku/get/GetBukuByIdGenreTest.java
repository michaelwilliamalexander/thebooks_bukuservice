package id.ac.ukdw.thebooksbookservice.features.buku.get;

import id.ac.ukdw.thebooksbookservice.controller.BukuController;
import id.ac.ukdw.thebooksbookservice.dto.response.BukuResponse;
import id.ac.ukdw.thebooksbookservice.dto.response.DetailBukuResponse;
import id.ac.ukdw.thebooksbookservice.model.Genre;
import id.ac.ukdw.thebooksbookservice.model.TableDetailBuku;
import id.ac.ukdw.thebooksbookservice.service.BukuService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetBukuByIdGenreTest {

    @InjectMocks
    private BukuController controller;

    @Mock
    private BukuService service;

    private MockMvc mockMvc;

    private List<DetailBukuResponse> dataBuku;

    private List<BukuResponse> response;

    private List<Genre> dataGenre;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataBuku = new ArrayList<>();
        response = new ArrayList<>();
        dataGenre = new ArrayList<>();
    }

    @Given("^List Data Detail Buku$")
    public void data(List<TableDetailBuku> data) {
        for (TableDetailBuku item: data){
            List<String>pengarang = Arrays.asList(item.getNamaPengarang().split("\\s*,\\s*"));
            List<String>genre = Arrays.asList(item.getNamaGenre().split("\\s*,\\s*"));
            dataBuku.add(new DetailBukuResponse(
                    item.getIsbn(),
                    item.getJudulBuku(),
                    pengarang,
                    genre,
                    item.getJumlahHalaman(),
                    item.getCoverBuku().getBytes(),
                    item.getSampleBuku().getBytes(),
                    item.getDeskripsiBuku(),
                    item.getTahunTerbit(),
                    Integer.valueOf(item.getJumlahHalaman()),
                    Double.valueOf(item.getHargaBuku()),
                    item.getNamaPenerbit()
            ));
        }
    }

    @Given("^Terlist Data Genre$")
    public void dataGenre(List<Genre> genre) {
        dataGenre = genre;
    }

    @When("^user ingin mendapatkan semua buku dengan id genre (.*)$")
    public void input(final String idGenre) {
        for (Genre data: dataGenre){
            if(idGenre.equals(data.getIdGenre())){
                for (DetailBukuResponse result:dataBuku){
                    for (String genre: result.getGenreBuku()){
                        if (genre.equals(data.getNamaGenre())){
                            response.add(new BukuResponse(
                                    result.getIsbn(),
                                    result.getJudulBuku(),
                                    result.getNamaPengarang(),
                                    result.getCoverBuku(),
                                    result.getStockBuku(),
                                    result.getHargaBuku()
                            ));
                            break;
                        }
                    }
                }
                when(service.findBukuByIdGenre(idGenre)).thenReturn(response);
            }
        }

    }
    @Then("^User akan menerima data buku (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) dari id Genre (.*)$")
    public void ouput(final String isbn,
                      final String judulBuku,
                      final String namaPengarang,
                      final String coverBuku,
                      final String namaPenerbit,
                      final String stockBuku,
                      final String hargaBuku,
                      final String idGenre) throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/buku/genre/{id}", idGenre)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data", hasSize(response.size())))
                .andDo(print());

        verify(service).findBukuByIdGenre(idGenre);
    }
}
