package id.ac.ukdw.thebooksbookservice.features.penulis.add;

import id.ac.ukdw.thebooksbookservice.config.ObjectMapping;
import id.ac.ukdw.thebooksbookservice.controller.PenulisController;
import id.ac.ukdw.thebooksbookservice.dto.request.PenulisRequest;
import id.ac.ukdw.thebooksbookservice.exception.BadRequestException;
import id.ac.ukdw.thebooksbookservice.model.Penulis;
import id.ac.ukdw.thebooksbookservice.service.PenulisService;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AddPenulisFailTest {

    @InjectMocks
    private PenulisController controller;

    @Mock
    private PenulisService service;

    private MockMvc mockMvc;

    private List<Penulis> penulis;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        penulis = new ArrayList<>();
    }

    @Given("^Terdapat data list penulis$")
    public void data(List<Penulis> data) {
        penulis = data;
    }


    @When("^User menginputkan data penulis (.*) dan (.*)$")
    public void input(final String idPenulis,
                      final String namaPenulis) {
        
        when(service.addPenulis(idPenulis,namaPenulis)).thenThrow(new BadRequestException());
    }
    @Then("^User akan mendapatkan pesan Bad Request dari data penulis (.*) dan (.*)$")
    public void output(final String idPenulis,
                       final String namaPenulis) throws Exception {
        
        mockMvc.perform(MockMvcRequestBuilders.post("/buku/penulis/")
                .content(ObjectMapping.asJsonString(new PenulisRequest(idPenulis,namaPenulis)))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
        
        verify(service).addPenulis(idPenulis,namaPenulis);
    }
}
