package id.ac.ukdw.thebooksbookservice.features.penulis.update;

import com.fasterxml.jackson.core.JsonProcessingException;
import id.ac.ukdw.thebooksbookservice.config.ObjectMapping;
import id.ac.ukdw.thebooksbookservice.controller.PenulisController;
import id.ac.ukdw.thebooksbookservice.dto.request.UpdatePenulisRequest;
import id.ac.ukdw.thebooksbookservice.model.Penulis;
import id.ac.ukdw.thebooksbookservice.service.PenulisService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UpdatePenulisTest {

    @InjectMocks
    private PenulisController controller;

    @Mock
    private PenulisService service;

    private MockMvc mockMvc;

    private List<Penulis> penulis;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        penulis = new ArrayList<>();
    }

    @Given("^Data list penulis$")
    public void data(List<Penulis> data) {
        penulis = data;
    }

    @When("^User mencari data penulis dengan menggunakan (.*) dan menginputkan data penulis (.*)$")
    public void input(final String idPenulis, final String namaPenulis) {
        when(service.updatePenulis(idPenulis,namaPenulis)).thenReturn("Update Berhasil");
    }

    @Then("^User akan mendapatkan pesan (.*) dari data update (.*) dan (.*)$")
    public void output(final String pesan,
                       final String idPenulis,
                       final String namaPenulis) throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.put("/buku/penulis/{id}",idPenulis)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectMapping.asJsonString(new UpdatePenulisRequest(namaPenulis))))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.data",is(pesan)));

        verify(service).updatePenulis(idPenulis,namaPenulis);
    }

}
