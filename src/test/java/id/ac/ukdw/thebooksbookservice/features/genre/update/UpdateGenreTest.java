package id.ac.ukdw.thebooksbookservice.features.genre.update;

import id.ac.ukdw.thebooksbookservice.config.ObjectMapping;
import id.ac.ukdw.thebooksbookservice.controller.GenreController;
import id.ac.ukdw.thebooksbookservice.dto.request.UpdateGenreRequest;
import id.ac.ukdw.thebooksbookservice.model.Genre;
import id.ac.ukdw.thebooksbookservice.service.GenreService;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


public class UpdateGenreTest {

    @InjectMocks
    private GenreController controller;

    @Mock
    private GenreService services;

    private MockMvc mockMvc;

    private List<Genre> genres;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        genres = new ArrayList<>();
    }

    @Given("^Terdapat data genre$")
    public void dataGenre(List<Genre> genreList) {
        genres = genreList;
    }

    @When("^User mempebarui data genre dengan id (.*) dengan data (.*)$")
    public void input(final String idGenre, final String namaGenre) {
        when(services.updateGenre(idGenre,namaGenre)).thenReturn("Update Berhasil");
    }

    @Then("^User mendapatkan pesan (.*) dari data update (.*) dan (.*)$")
    public void output(final String pesan,final String idGenre, final String genreBaru) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.put("/buku/genre/{id}", idGenre)
                .content(ObjectMapping.asJsonString(new UpdateGenreRequest(genreBaru)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.data",is(pesan)));

        verify(services).updateGenre(idGenre,genreBaru);
    }
}
