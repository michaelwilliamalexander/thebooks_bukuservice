package id.ac.ukdw.thebooksbookservice.features.buku.add;

import id.ac.ukdw.thebooksbookservice.config.ObjectMapping;
import id.ac.ukdw.thebooksbookservice.controller.BukuController;
import id.ac.ukdw.thebooksbookservice.dto.request.BukuRequest;
import id.ac.ukdw.thebooksbookservice.dto.response.BukuResponse;
import id.ac.ukdw.thebooksbookservice.dto.response.DetailBukuResponse;
import id.ac.ukdw.thebooksbookservice.exception.BadRequestException;
import id.ac.ukdw.thebooksbookservice.model.Genre;
import id.ac.ukdw.thebooksbookservice.model.TableDetailBuku;
import id.ac.ukdw.thebooksbookservice.service.BukuService;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.SneakyThrows;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AddBukuFailTest {

    @InjectMocks
    private BukuController controller;

    @Mock
    private BukuService service;

    private MockMvc mockMvc;

    private List<DetailBukuResponse> dataBuku;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataBuku = new ArrayList<>();
    }

    @Given("^Terdapat data list-list Buku$")
    public void data(List<TableDetailBuku> data) {
        for (TableDetailBuku item: data){
            List<String>pengarang = Arrays.asList(item.getNamaPengarang().split("\\s*,\\s*"));
            List<String>genre = Arrays.asList(item.getNamaGenre().split("\\s*,\\s*"));
            dataBuku.add(new DetailBukuResponse(
                    item.getIsbn(),
                    item.getJudulBuku(),
                    pengarang,
                    genre,
                    item.getJumlahHalaman(),
                    item.getCoverBuku().getBytes(),
                    item.getSampleBuku().getBytes(),
                    item.getDeskripsiBuku(),
                    item.getTahunTerbit(),
                    Integer.valueOf(item.getJumlahHalaman()),
                    Double.valueOf(item.getHargaBuku()),
                    item.getNamaPenerbit()
            ));
        }
    }

    @SneakyThrows
    @When("^User memasukkan data buku (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*)$")
    public void input(final String isbn,
                      final String judulBuku,
                      final String namaPengarang,
                      final String coverBuku,
                      final String sampleBuku,
                      final String namaPenerbit,
                      final String deskripsiBuku,
                      final String namaGenre,
                      final int jumlahHalaman,
                      final String tahunTerbit,
                      final int stockBuku,
                      final double hargaBuku){
        List<String>genre = Arrays.asList(namaGenre.split("\\s*,\\s*"));
        List<String>penulis = Arrays.asList(namaPengarang.split("\\s*,\\s*"));
        MultipartFile imageCover = new MockMultipartFile(
                "coverBuku",
                coverBuku,
                MediaType.MULTIPART_FORM_DATA_VALUE,
                coverBuku.getBytes());

        MultipartFile bukuSample = new MockMultipartFile(
                "sampleBuku",
                sampleBuku,
                MediaType.MULTIPART_FORM_DATA_VALUE,
                sampleBuku.getBytes());

        when(service.saveBuku(
                isbn,
                judulBuku,
                deskripsiBuku,
                hargaBuku,
                stockBuku,
                jumlahHalaman,
                tahunTerbit,
                genre,
                penulis,
                namaPenerbit,
                imageCover.getBytes(),
                bukuSample.getBytes()
        )).thenThrow(new BadRequestException());
    }

    @SneakyThrows
    @Then("^User mendapatkan pesan Bad Request dari data buku (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) . (.*) yang diinputkan$")
    public void output(final String isbn,
                       final String judulBuku,
                       final String namaPengarang,
                       final String coverBuku,
                       final String sampleBuku,
                       final String namaPenerbit,
                       final String deskripsiBuku,
                       final String namaGenre,
                       final int jumlahHalaman,
                       final String tahunTerbit,
                       final int stockBuku,
                       final double hargaBuku){
        List<String>genre = Arrays.asList(namaGenre.split("\\s*,\\s*"));
        List<String>penulis = Arrays.asList(namaPengarang.split("\\s*,\\s*"));
        MockMultipartFile jsonFile =
                new MockMultipartFile(
                        "dataBuku",
                        "",
                        "application/json",
                        ObjectMapping.asJsonString(new BukuRequest(
                                isbn,
                                judulBuku,
                                deskripsiBuku,
                                tahunTerbit,
                                stockBuku,
                                hargaBuku,
                                jumlahHalaman,
                                genre,
                                penulis,
                                namaPenerbit
                        )).getBytes());

        MockMultipartFile imageCover =
                new MockMultipartFile(
                        "coverBuku",
                        coverBuku,
                        MediaType.MULTIPART_FORM_DATA_VALUE,
                        coverBuku.getBytes());

        MockMultipartFile bukuSample = new MockMultipartFile(
                "sampleBuku",
                sampleBuku,
                MediaType.MULTIPART_FORM_DATA_VALUE,
                sampleBuku.getBytes());

        mockMvc.perform(MockMvcRequestBuilders.multipart("/buku/")
                .file(jsonFile)
                .file(imageCover)
                .file(bukuSample))
                .andDo(print())
                .andExpect(status().isBadRequest());

        verify(service).saveBuku(
                isbn,
                judulBuku,
                deskripsiBuku,
                hargaBuku,
                stockBuku,
                jumlahHalaman,
                tahunTerbit,
                genre,
                penulis,
                namaPenerbit,
                imageCover.getBytes(),
                bukuSample.getBytes());
    }


}
