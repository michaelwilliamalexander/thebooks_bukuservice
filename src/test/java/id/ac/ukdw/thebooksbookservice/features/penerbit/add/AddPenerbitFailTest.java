package id.ac.ukdw.thebooksbookservice.features.penerbit.add;

import com.fasterxml.jackson.core.JsonProcessingException;
import id.ac.ukdw.thebooksbookservice.config.ObjectMapping;
import id.ac.ukdw.thebooksbookservice.controller.PenerbitController;
import id.ac.ukdw.thebooksbookservice.dto.request.PenerbitRequest;
import id.ac.ukdw.thebooksbookservice.exception.BadRequestException;
import id.ac.ukdw.thebooksbookservice.model.Penerbit;
import id.ac.ukdw.thebooksbookservice.service.PenerbitService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AddPenerbitFailTest {

    @InjectMocks
    private PenerbitController controller;

    @Mock
    private PenerbitService service;

    private MockMvc mockMvc;

    private List<Penerbit> penerbits;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        penerbits = new ArrayList<>();
    }

    @Given("^Terdapat beberapa data-data penerbit$")
    public void data(List<Penerbit> dataPenerbit) {
        penerbits = dataPenerbit;
    }

    @When("^User akan memasukkan data penerbit (.*) , (.*)$")
    public void input(final String idPenerbit, final String namaPenerbit) {
        when(service.addPenerbit(idPenerbit,namaPenerbit))
                .thenThrow(new BadRequestException());
    }
    @Then("^User akan mendapatkan pesan Bad Request dari inputan data penerbit (.*) , (.*)$")
    public void output(final String idPenerbit, final String namaPenerbit) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/buku/penerbit/")
                .content(ObjectMapping.asJsonString
                        (new PenerbitRequest(idPenerbit,namaPenerbit)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verify(service).addPenerbit(idPenerbit,namaPenerbit);
    }
}
