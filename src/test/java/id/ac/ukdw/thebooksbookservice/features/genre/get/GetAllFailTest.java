package id.ac.ukdw.thebooksbookservice.features.genre.get;

import id.ac.ukdw.thebooksbookservice.controller.GenreController;
import id.ac.ukdw.thebooksbookservice.exception.NotFoundException;
import id.ac.ukdw.thebooksbookservice.service.GenreService;
import io.cucumber.java.Before;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetAllFailTest {

    @InjectMocks
    private GenreController controller;

    @Mock
    private GenreService services;

    private MockMvc mockMvc;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @When("User menampilkan semua data genre")
    public void input() {
        when(services.getAllGenre()).thenThrow(new NotFoundException());
    }

    @Then("User mendapatkan pesan Not Found terhadap data genre")
    public void output() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/buku/genre/"))
                .andExpect(status().isNotFound())
                .andDo(print());

        verify(services).getAllGenre();
    }


}
