package id.ac.ukdw.thebooksbookservice.features.buku.get;

import id.ac.ukdw.thebooksbookservice.config.ObjectMapping;
import id.ac.ukdw.thebooksbookservice.controller.BukuController;
import id.ac.ukdw.thebooksbookservice.dto.response.BukuResponse;
import id.ac.ukdw.thebooksbookservice.dto.response.DetailBukuResponse;
import id.ac.ukdw.thebooksbookservice.dto.response.ResponseWrapper;
import id.ac.ukdw.thebooksbookservice.model.TableDetailBuku;
import id.ac.ukdw.thebooksbookservice.service.BukuService;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetDetailBukuTest {

    @InjectMocks
    private BukuController controller;

    @Mock
    private BukuService service;

    private MockMvc mockMvc;

    private List<DetailBukuResponse> dataBuku;

    private DetailBukuResponse response;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataBuku = new ArrayList<>();
    }

    @Given("Terdapat data Buku")
    public void data(List<TableDetailBuku> data) {
        for (TableDetailBuku item: data){
            List<String>pengarang = Arrays.asList(item.getNamaPengarang().split("\\s*,\\s*"));
            List<String>genre = Arrays.asList(item.getNamaGenre().split("\\s*,\\s*"));
            dataBuku.add(new DetailBukuResponse(
                    item.getIsbn(),
                    item.getJudulBuku(),
                    pengarang,
                    genre,
                    item.getJumlahHalaman(),
                    item.getCoverBuku().getBytes(),
                    item.getSampleBuku().getBytes(),
                    item.getDeskripsiBuku(),
                    item.getTahunTerbit(),
                    Integer.valueOf(item.getJumlahHalaman()),
                    Double.valueOf(item.getHargaBuku()),
                    item.getNamaPenerbit()
            ));
        }
    }

    @When("^User ingin mendapatkan detail buku berisbn (.*)$")
    public void input(final String isbn) {
        for (DetailBukuResponse data:dataBuku){
            if (data.getIsbn().equals(isbn)){
                response = data;
                when(service.getDetailBuku(isbn)).thenReturn(data);
            }
        }
    }
    @Then("^User mendapatkan detail buku berdasarkan (.*)$")
    public void output(final String isbn) throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/buku/detail")
                .contentType(MediaType.APPLICATION_JSON)
                .param("isbn",isbn))
                .andExpect(jsonPath("$.data.isbn",is(response.getIsbn())))
                .andExpect(jsonPath("$.data.judulBuku",is(response.getJudulBuku())))
                .andExpect(jsonPath("$.data.genreBuku",hasSize(response.getGenreBuku().size())))
                .andExpect(jsonPath("$.data.namaPenerbit",is(response.getNamaPenerbit())))
                .andExpect(content().bytes(
                        ObjectMapping.asJsonString(
                                new ResponseWrapper(
                                        response)).getBytes()))
                .andExpect(status().isOk()).andDo(print());

        verify(service).getDetailBuku(isbn);
    }
}
