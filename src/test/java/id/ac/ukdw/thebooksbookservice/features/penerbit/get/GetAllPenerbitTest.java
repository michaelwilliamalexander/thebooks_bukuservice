package id.ac.ukdw.thebooksbookservice.features.penerbit.get;

import id.ac.ukdw.thebooksbookservice.controller.PenerbitController;
import id.ac.ukdw.thebooksbookservice.dto.PenerbitDTO;
import id.ac.ukdw.thebooksbookservice.model.Penerbit;
import id.ac.ukdw.thebooksbookservice.service.PenerbitService;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetAllPenerbitTest {

    @InjectMocks
    private PenerbitController controller;

    @Mock
    private PenerbitService service;

    private MockMvc mockMvc;

    private List<PenerbitDTO> penerbits;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        penerbits = new ArrayList<>();
    }

    @Given("Terdapat data-data penerbit di dalam sistem")
    public void data(List<PenerbitDTO> penerbitList) {
        penerbits = penerbitList;
    }

    @When("User ingin menampilkan data penerbit")
    public void input() {
        when(service.getAllPenerbit()).thenReturn(penerbits);
    }

    @Then("User akan mendapatkan semua data penerbit")
    public void output() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/buku/penerbit/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data",hasSize(penerbits.size())))
                .andExpect(jsonPath("$.data[0].idPenerbit",is(penerbits.get(0).getIdPenerbit())))
                .andDo(print());

        verify(service).getAllPenerbit();
    }
}
