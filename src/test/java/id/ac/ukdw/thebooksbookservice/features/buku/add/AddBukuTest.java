package id.ac.ukdw.thebooksbookservice.features.buku.add;

import id.ac.ukdw.thebooksbookservice.config.ObjectMapping;
import id.ac.ukdw.thebooksbookservice.controller.BukuController;
import id.ac.ukdw.thebooksbookservice.dto.request.BukuRequest;
import id.ac.ukdw.thebooksbookservice.dto.response.DetailBukuResponse;
import id.ac.ukdw.thebooksbookservice.model.TableDetailBuku;
import id.ac.ukdw.thebooksbookservice.service.BukuService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import lombok.SneakyThrows;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class AddBukuTest {
    @InjectMocks
    private BukuController controller;

    @Mock
    private BukuService service;

    private MockMvc mockMvc;

    private List<DetailBukuResponse> dataBuku;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataBuku = new ArrayList<>();
    }

    @Given("^Terdapat data list Buku$")
    public void data(List<TableDetailBuku> buku){
        for (TableDetailBuku item: buku){
            List<String>pengarang = Arrays.asList(item.getNamaPengarang().split("\\s*,\\s*"));
            List<String>genre = Arrays.asList(item.getNamaGenre().split("\\s*,\\s*"));
            dataBuku.add(new DetailBukuResponse(
                    item.getIsbn(),
                    item.getJudulBuku(),
                    pengarang,
                    genre,
                    item.getJumlahHalaman(),
                    item.getCoverBuku().getBytes(),
                    item.getSampleBuku().getBytes(),
                    item.getDeskripsiBuku(),
                    item.getTahunTerbit(),
                    Integer.valueOf(item.getJumlahHalaman()),
                    Double.valueOf(item.getHargaBuku()),
                    item.getNamaPenerbit()
            ));
        }
    }

    @SneakyThrows
    @When("^User menginputkan data buku (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*)$")
    public void input(final String isbn,
                      final String judulBuku,
                      final String namaPengarang,
                      final String coverBuku,
                      final String deskripsiBuku,
                      final String sampleBuku,
                      final String namaPenerbit,
                      final String namaGenre,
                      final String tahunTerbit,
                      final int jumlahHalaman,
                      final double hargaBuku,
                      final int stockBuku){

        List<String>genre = Arrays.asList(namaGenre.split("\\s*,\\s*"));
        List<String>penulis = Arrays.asList(namaPengarang.split("\\s*,\\s*"));
        MultipartFile imageCover = new MockMultipartFile(
                "coverBuku",
                coverBuku,
                MediaType.MULTIPART_FORM_DATA_VALUE,
                coverBuku.getBytes());

        MultipartFile bukuSample = new MockMultipartFile(
                "sampleBuku",
                sampleBuku,
                MediaType.MULTIPART_FORM_DATA_VALUE,
                sampleBuku.getBytes());
        for (DetailBukuResponse data: dataBuku){
            if (!isbn.equals(data.getIsbn())){
                when(service.saveBuku(
                        isbn,
                        judulBuku,
                        deskripsiBuku,
                        hargaBuku,
                        stockBuku,
                        jumlahHalaman,
                        tahunTerbit,
                        genre,
                        penulis,
                        namaPenerbit,
                        imageCover.getBytes(),
                        bukuSample.getBytes()
                )).thenReturn("Penyimpanan Berhasil");
            }
        }

    }

    @SneakyThrows
    @Then("^User mendapatkan pesan (.*) dari data buku (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*)$")
    public void output(final String pesan,
                       final String isbn,
                       final String judulBuku,
                       final String namaPengarang,
                       final String coverBuku,
                       final String deskripsiBuku,
                       final String sampleBuku,
                       final String namaPenerbit,
                       final String namaGenre,
                       final String tahunTerbit,
                       final int jumlahHalaman,
                       final double hargaBuku,
                       final int stockBuku){

        List<String>genre = Arrays.asList(namaGenre.split("\\s*,\\s*"));
        List<String>penulis = Arrays.asList(namaPengarang.split("\\s*,\\s*"));
        MockMultipartFile jsonFile =
                new MockMultipartFile(
                        "dataBuku",
                        "",
                        "application/json",
                        ObjectMapping.asJsonString(new BukuRequest(
                                isbn,
                                judulBuku,
                                deskripsiBuku,
                                tahunTerbit,
                                stockBuku,
                                hargaBuku,
                                jumlahHalaman,
                                genre,
                                penulis,
                                namaPenerbit
                        )).getBytes());

        MockMultipartFile imageCover =
                new MockMultipartFile(
                        "coverBuku",
                        coverBuku,
                        MediaType.MULTIPART_FORM_DATA_VALUE,
                        coverBuku.getBytes());

        MockMultipartFile bukuSample = new MockMultipartFile(
                "sampleBuku",
                sampleBuku,
                MediaType.MULTIPART_FORM_DATA_VALUE,
                sampleBuku.getBytes());

        mockMvc.perform(MockMvcRequestBuilders.multipart("/buku/")
                .file(jsonFile)
                .file(imageCover)
                .file(bukuSample))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data",is(pesan)));

        verify(service).saveBuku(
                isbn,
                judulBuku,
                deskripsiBuku,
                hargaBuku,
                stockBuku,
                jumlahHalaman,
                tahunTerbit,
                genre,
                penulis,
                namaPenerbit,
                imageCover.getBytes(),
                bukuSample.getBytes());
    }
}
