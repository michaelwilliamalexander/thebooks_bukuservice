package id.ac.ukdw.thebooksbookservice.features.buku.get;

import id.ac.ukdw.thebooksbookservice.controller.BukuController;
import id.ac.ukdw.thebooksbookservice.dto.response.BukuResponse;
import id.ac.ukdw.thebooksbookservice.dto.response.DetailBukuResponse;
import id.ac.ukdw.thebooksbookservice.model.TableDetailBuku;
import id.ac.ukdw.thebooksbookservice.service.BukuService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class SearchBukuTest {

    @InjectMocks
    private BukuController controller;

    @Mock
    private BukuService service;

    private MockMvc mockMvc;

    private List<DetailBukuResponse> dataBuku;

    private List<BukuResponse> response;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataBuku = new ArrayList<>();
        response = new ArrayList<>();
    }

    @Given("^List data detail Buku$")
    public void data(List<TableDetailBuku> data) {
        for (TableDetailBuku item: data){
            List<String>pengarang = Arrays.asList(item.getNamaPengarang().split("\\s*,\\s*"));
            List<String>genre = Arrays.asList(item.getNamaGenre().split("\\s*,\\s*"));
            dataBuku.add(new DetailBukuResponse(
                    item.getIsbn(),
                    item.getJudulBuku(),
                    pengarang,
                    genre,
                    item.getJumlahHalaman(),
                    item.getCoverBuku().getBytes(),
                    item.getSampleBuku().getBytes(),
                    item.getDeskripsiBuku(),
                    item.getTahunTerbit(),
                    Integer.valueOf(item.getJumlahHalaman()),
                    Double.valueOf(item.getHargaBuku()),
                    item.getNamaPenerbit()
            ));
        }
    }

    @When("^user mencari data buku dengan keyword (.*)$")
    public void input(String keyword) {
        for(DetailBukuResponse data: dataBuku){
            for (String pengarang: data.getNamaPengarang()){
                if (pengarang.toLowerCase().contains(keyword.toLowerCase()) ||
                        data.getJudulBuku().toLowerCase().contains(keyword.toLowerCase())){
                    response.add(new BukuResponse(
                            data.getIsbn(),
                            data.getJudulBuku(),
                            data.getNamaPengarang(),
                            data.getCoverBuku(),
                            data.getStockBuku(),
                            data.getHargaBuku()     
                    ));
                    break;
                }
            }
        }
        when(service.searchBuku(keyword)).thenReturn(response);
    }

    @Then("^User akan mendapatkan data buku (.*) , (.*) , (.*) , (.*) , (.*) , (.*) , (.*) dengan keyword (.*)$")
    public void output(final String isbn,
                       final String judulBuku,
                       final String coverBuku,
                       final String pengarang,
                       final String penerbit,
                       final String stockBuku,
                       final String hargaBuku,
                       final String keyword) throws Exception{

        mockMvc.perform(MockMvcRequestBuilders.get("/buku/search/")
                .param("keyword",keyword)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data",hasSize(response.size())))
                .andDo(print());

        verify(service).searchBuku(keyword);
    }
}
