package id.ac.ukdw.thebooksbookservice.features.penulis.delete;

import id.ac.ukdw.thebooksbookservice.controller.PenulisController;
import id.ac.ukdw.thebooksbookservice.model.Penulis;
import id.ac.ukdw.thebooksbookservice.service.PenulisService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DeletePenulisTest {

    @InjectMocks
    private PenulisController controller;

    @Mock
    private PenulisService service;

    private MockMvc mockMvc;

    private List<Penulis> penulis;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        penulis = new ArrayList<>();
    }

    @Given("^Terdapat list data penulis$")
    public void data(List<Penulis> data) {
        penulis = data;
    }

    @When("^User ingin menghapus data penulis dengan id (.*)$")
    public void input(final String idPenulis) {
        when(service.deletePenulis(idPenulis)).thenReturn("Delete Berhasil");
    }
    
    @Then("^User mendapatkan pesan (.*) dari id Penulis (.*)$")
    public void output(final String pesan,
                       final String idPenulis) throws Exception {

         mockMvc.perform(MockMvcRequestBuilders.delete("/buku/penulis/{id}",idPenulis))
                 .andExpect(status().isOk())
                 .andExpect(jsonPath("$.data",is(pesan)));
         
         verify(service).deletePenulis(idPenulis);
    }
}
