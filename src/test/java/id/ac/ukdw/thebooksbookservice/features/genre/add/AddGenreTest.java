package id.ac.ukdw.thebooksbookservice.features.genre.add;

import id.ac.ukdw.thebooksbookservice.config.ObjectMapping;
import id.ac.ukdw.thebooksbookservice.controller.GenreController;
import id.ac.ukdw.thebooksbookservice.dto.request.GenreRequest;
import id.ac.ukdw.thebooksbookservice.model.Genre;
import id.ac.ukdw.thebooksbookservice.service.GenreService;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class AddGenreTest {

    @InjectMocks
    private GenreController controller;

    @Mock
    private GenreService services;

    private MockMvc mockMvc;

    List<Genre> genres;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        genres = new ArrayList<>();
    }

    @Given("^Terdapat data genre di dalam sistem$")
    public void data(List<Genre> genreList) {
        genres = genreList;
    }

    @When("^User menginputkan data genre (.*) dan (.*)$")
    public void input(final String idGenre, final String namaGenre) {
        Mockito.when(services.addGenre(idGenre,namaGenre)).thenReturn("Penyimpanan Berhasil");
    }

    @Then("^User akan menerima (.*) dari data genre (.*) dan (.*)$")
    public void output(final String pesan, final String idGenre, final String namaGenre) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/buku/genre/")
                .content(ObjectMapping.asJsonString(new GenreRequest(idGenre,namaGenre)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data",is(pesan)))
                .andDo(print());

        Mockito.verify(services).addGenre(idGenre,namaGenre);
    }
}
