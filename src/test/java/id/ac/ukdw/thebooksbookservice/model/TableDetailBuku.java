package id.ac.ukdw.thebooksbookservice.model;

import lombok.Data;

import java.util.List;

@Data
public class TableDetailBuku {

    private String isbn;

    private String judulBuku;

    private String namaPengarang;

    private String namaGenre;

    private int jumlahHalaman;

    private String coverBuku;

    private String sampleBuku;

    private String deskripsiBuku;

    private String tahunTerbit;

    private int stockBuku;

    private double hargaBuku;

    private String namaPenerbit;
}
