package id.ac.ukdw.thebooksbookservice.model;

import lombok.Data;

import java.util.List;

@Data
public class TableBuku {

    private String isbn;

    private String judulBuku;

    private String namaPengarang;

    private String coverBuku;

    private double hargaBuku;

    private int stockBuku;
}
